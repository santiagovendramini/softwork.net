﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class logmon : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["css"] == "1")
        {
            Response.ContentType = "text/css";
            foreach (Pattern pat in this.initPatterns())
            {
                Response.Write(pat.toCSS() + "\n");
            }
            Response.End();
            return;
        }

        Server.ScriptTimeout = 30;
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        String filename = Logger.CurrentFile();
        List<Pattern> styles = this.initPatterns();

        try
        {
            if (filename != null)
            {
                long lastsize = filesize(filename);
                //long lastsize = 0;

                Pattern pattern = null;
                String line = null;
                FileStream fs = null;
                StreamReader sr = null;
                bool exit = false;

                while (!exit)
                {
                    try
                    {
                        long currentsize = filesize(filename);

                        if (lastsize < currentsize)
                        {
                            fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                            fs.Seek(lastsize, SeekOrigin.Begin);
                            sr = new StreamReader(fs);
                            while (sr.Peek() > 0)
                            {
                                line = sr.ReadLine();
                                pattern = this.clasify(styles, line);
                                if (pattern != null)
                                    Response.Write(pattern.toJson());
                            }
                            Response.Flush();
                            sr.Close();
                            fs.Close();
                            sr = null;
                            fs = null;
                            lastsize = currentsize;
                            System.Threading.Thread.Sleep(500);
                        }
                        else
                        {
                            System.Threading.Thread.Sleep(1000);
                        }
                    }
                    catch (Exception ex)
                    {
                        Response.Write(ex.Message);
                        exit = true;
                    }

                }
            }
            else
            {
                Response.StatusCode = 500;
                Response.Write("Unable to open Log file");
            }
        }
        catch (Exception ex)
        {
            Response.StatusCode = 500;
            Response.Write(ex.Message);
        }
    }

    private long filesize(String filename)
    {
        FileInfo f = new FileInfo(filename);
        return f.Length;
    }

    private List<Pattern> initPatterns()
    {
        List<Pattern> list = new List<Pattern>();
        //[29/01/2015 14:51:58] A 4e6503ac-f965-4943-8679-5c77a0d2205a Response 200 OK
        list.Add(new Pattern("Activity", @"\[[\d\/]+ [\d:]+\] A", "#1632D0"));
        list.Add(new Pattern("Error", @"\[[\d\/]+ [\d:]+\] E", "#FF0000"));
        list.Add(new Pattern("Db", @"\[[\d\/]+ [\d:]+\] S", "#03A9F4"));
        list.Add(new Pattern("Debug", @"\[[\d\/]+ [\d:]+\] D", "#808080"));
        list.Add(new Pattern("Timing", @"\[[\d\/]+ [\d:]+\] T", "#016501"));

        return list;
    }

    private Pattern clasify(List<Pattern> patterns, String line)
    {
        foreach (Pattern pat in patterns)
        {
            if (pat.matches(line))
                return pat;
        }
        return null;
    }

    /// <summary>
    /// uses 'head' and 'tail' -- 'head' has already been pattern-expanded
    /// and 'tail' has not.
    /// </summary>
    /// <param name="head">wildcard-expanded</param>
    /// <param name="tail">not yet wildcard-expanded</param>
    /// <returns></returns>
    public static IEnumerable<string> Glob(string head, string tail)
    {
        if (PathTail(tail) == tail)
            foreach (string path in Directory.GetFiles(head, tail).OrderBy(s => s))
                yield return path;
        else
            foreach (string dir in Directory.GetDirectories(head, PathHead(tail)).OrderBy(s => s))
                foreach (string path in Glob(Path.Combine(head, dir), PathTail(tail)))
                    yield return path;
    }

    /// <summary>
    /// shortcut
    /// </summary>
    static char DirSep = Path.DirectorySeparatorChar;

    /// <summary>
    /// return the first element of a file path
    /// </summary>
    /// <param name="path">file path</param>
    /// <returns>first logical unit</returns>
    static string PathHead(string path)
    {
        // handle case of \\share\vol\foo\bar -- return \\share\vol as 'head'
        // because the dir stuff won't let you interrogate a server for its share list
        // FIXME check behavior on Linux to see if this blows up -- I don't think so
        if (path.StartsWith("" + DirSep + DirSep))
            return path.Substring(0, 2) + path.Substring(2).Split(DirSep)[0] + DirSep + path.Substring(2).Split(DirSep)[1];

        return path.Split(DirSep)[0];
    }

    /// <summary>
    /// return everything but the first element of a file path
    /// e.g. PathTail("C:\TEMP\foo.txt") = "TEMP\foo.txt"
    /// </summary>
    /// <param name="path">file path</param>
    /// <returns>all but the first logical unit</returns>
    static string PathTail(string path)
    {
        if (!path.Contains(DirSep))
            return path;

        return path.Substring(1 + PathHead(path).Length);
    }
}