﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Web;
using System.Web.Helpers;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading;

/// <summary>
/// Summary description for Request
/// </summary>
public class RequestWrapper
{
    private Dictionary<string, Stopwatch> timers;
    public String verb;
    public String full_page;
    public String page;
    public String body;
    public String request_type;
    public String response_type;
    public String template;
    public String redirect;
    public String[] url_elements;
    public Dictionary<string, string> parameters;
    public Dictionary<string, string> session;
    public Dictionary<string, object> response;
    public Dictionary<string, string> headers;
    public Parser parser;
    public HttpRequest request;
    public HttpResponse response_obj;
    public GearUp_API.Usuarios user;
    public CultureInfo culture;
    public object parameters_json;
    public int status_code;
    public string status_msg;
    public bool user_updated;
    public dynamic client;
    public bool mobile;
    public string lang;

    public RequestWrapper(HttpRequest pRequest, HttpResponse pResponse)
    {
        this.timers = new Dictionary<string, Stopwatch>();
        this.timer_start("response");

        this.user_updated = false;
        this.redirect = "";
        this.response_obj = pResponse;
        this.request = pRequest;
        this.parser = null;
        this.parameters_json = null;
        this.user = null;
        this.status_code = 200;
        this.status_msg = "OK";
        this.client = null;
        this.mobile = false;
        this.init();
    }

    public long close()
    {
        return this.timer_stop("response");
    }

    public void init()
    {
        //INITIALIZATION
        this.verb = request.HttpMethod;
        this.culture = Thread.CurrentThread.CurrentCulture;
        this.response = new Dictionary<string, object>();
        this.headers = new Dictionary<string, string>();
        this.parameters = new Dictionary<string, string>();
        this.session = new Dictionary<string, string>();
        this.full_page = this.request.RawUrl;
        this.full_page = this.full_page.ToLower().StartsWith(ConfigManager.GET("SiteBasePath").ToLower()) ? this.full_page.Substring(ConfigManager.GET("SiteBasePath").Length) : this.full_page;
        this.page = this.full_page.IndexOf('?') > -1 ? this.full_page.Remove(this.full_page.IndexOf('?')) : this.full_page;
        //this.url_elements = this.page.Split(new char[1] { '/' });
        //this.url_elements[0] = this.page;
        Logger.Initialize();
        Logger.Activity("Request - init: " + this.verb + " " + this.page);
        if (this.request.UserAgent != null && (this.request.UserAgent.Contains("Android") || this.request.UserAgent.Contains("iPhone")))
        {
            this.mobile = true;
            Logger.Debug("Request - init: Mobile UA");
        }

        //get request type
        this.request_type = this.request.ContentType.ToLower();
        this.request_type = this.request_type.IndexOf(';') > -1 ? this.request_type.Remove(this.request_type.IndexOf(';')) : this.request_type;

        this.response_type = "application/json";
        //get response type
        if (this.request.AcceptTypes != null && this.request.AcceptTypes.Length > 0)
        {
            this.response_type = String.Join(";", this.request.AcceptTypes).ToLower();
            this.response_type = this.response_type.IndexOf(';') > -1 ? this.response_type.Remove(this.response_type.IndexOf(';')) : this.response_type;
        }

        Logger.Activity("Request - init: Request type: " + this.request_type + " - Response type: " + this.response_type);

        //get request body content
        try
        {
            StreamReader sr = new StreamReader(this.request.InputStream);
            this.body = sr.ReadToEnd();
        }
        catch (Exception e)
        {
            this.body = "";
            Logger.Error("Request - init: " + e.Message);
        }

        //PARSE PARAMETERS
        //query string parameters
        foreach (string param in this.request.QueryString)
        {
            this.parameters.Add(param, this.request.QueryString[param]);
        }
        this.parameters.Remove("nocache");

        //body parameters
        Parser parser = null;
        if (this.body.Length > 0)
        {
            switch (this.request_type)
            {
                case "application/json":
                    this.parameters_json = Newtonsoft.Json.JsonConvert.DeserializeObject(this.body);
                    parser = new JsonParser(this);
                    DynamicJsonObject tmp2 = (DynamicJsonObject)parser.decode(this.body);
                    IEnumerator<String> i = tmp2.GetDynamicMemberNames().GetEnumerator();
                    while (i.MoveNext())
                    {
                        object value;
                        JsonMemberBinder binder = new JsonMemberBinder(i.Current, false);
                        tmp2.TryGetMember(binder, out value);
                        this.parameters.Add(i.Current, value.ToString());
                    }
                    Logger.Debug("Request - init: Body parameters using " + parser.GetType());
                    break;
                case "text/html":
                case "application/x-www-form-urlencoded":
                    parser = new HtmlParser(this);
                    NameValueCollection tmp = (NameValueCollection)parser.decode(this.body);
                    foreach (String param in tmp)
                    {
                        if (this.parameters.ContainsKey(param))
                            this.parameters[param] = tmp[param];
                        else
                            this.parameters.Add(param, tmp[param]);
                    }
                    Logger.Debug("Request - init: Body parameters using " + parser.GetType());
                    break;
                default:
                    break;
            }
        }

        foreach(string key in this.parameters.Keys)
        {
            Logger.Debug("Request - init: " + key + ": " + this.parameters[key]);
        }

        //VERIFY RESPONSE TYPE
        switch (this.response_type)
        {
            case "application/json":
                //Response.ContentType = "application/json";
                this.parser = new JsonParser(this);
                Logger.Debug("Request - init: Using " + this.parser.GetType());
                break;
            case "text/html":
            default:
                this.parser = new HtmlParser(this);
                Logger.Debug("Request - init: Using " + this.parser.GetType());
                break;
        }
    }

    public void render()
    {
        if (this.parser != null)
        {
            this.parser.render();
        }
        this.response_obj.StatusCode = this.status_code;
        this.response_obj.StatusDescription = this.status_msg;
    }

    public void error(string msg)
    {
        Logger.Error(msg);
        if (this.parser != null)
        {
            this.parser.error(msg);
        }
        this.response_obj.StatusCode = this.status_code;
        this.response_obj.StatusDescription = this.status_msg;
    }

    public void reload_usuario()
    {
        if (!oAuth.SetSession(this))
        {
            throw new Exception("WEB_ERROR_USERS_1");
        }
        this.user_updated = true;
    }

    public bool user2session(GearUp_API.Usuarios user)
    {
        this.timer_start("request_session");
        this.user = user;
        string[] excluded = { "Pass" };
        System.Reflection.PropertyInfo[] props = user.GetType().GetProperties();
        foreach (var prop in props)
        {
            if (!Array.Exists(excluded, element => element == prop.Name))
            {
                object value = prop.GetValue(user, null);
                //Logger.Debug("Usuario property " + prop.Name + ": " + (value != null ? value.ToString() : "null"));
                if (this.session.ContainsKey(prop.Name))
                {
                    this.session[prop.Name] = (value != null ? value.ToString() : null);
                }
                else
                {
                    this.session.Add(prop.Name, (value != null ? value.ToString() : null));
                }
            }
        }

        //obtener credito regalado
        /*object credito = Dispatcher.dispatchDLL("CreditosRegaladosBL::ObtenerCreditoRegaloDisponible", new object[] { user }, new object[] { "ARG", "ES" });
        if (credito != null)
        {
            if (this.session.ContainsKey("CreditoRegalado"))
            {
                this.session["CreditoRegalado"] = credito.ToString();
            }
            else
            {
                this.session.Add("CreditoRegalado", credito.ToString());
            }
        }
        else
        {
            Logger.Error("No fue posible obtener credito regalado");
        }*/

        //verificar cultura según país del usuario
        /*this.culture = Paises.getCountryCulture((int)user.PaisID);
        if (this.session.ContainsKey("DatePattern"))
        {
            this.session["DatePattern"] = this.culture.DateTimeFormat.ShortDatePattern;
        }
        else
        {
            this.session.Add("DatePattern", this.culture.DateTimeFormat.ShortDatePattern);
        }*/

        this.timer_stop("request_session");
        return true;

        //WalletFunction.BL.PaisesBL bl = new WalletFunction.BL.PaisesBL();
        //WalletFunction.Entities.Paises pais = bl.ObtenerPorId((int)user.PaisID);
        /*WalletFunction.Entities.Paises pais = (WalletFunction.Entities.Paises)Dispatcher.dispatchDLL("PaisesBL::ObtenerPorId", new object[] { (int)user.PaisID }, new object[] { "ARG", "ES" });
        if (pais != null) {
            //string iso2 = bl.ObtenerPorId((int)user.PaisID).ISO_Alpha2Code;
            string iso2 = "es-" + pais.ISO_Alpha2Code;
            //List<CultureInfo> cultures = CultureInfo.GetCultures(CultureTypes.AllCultures).Where(c => c.Name.EndsWith(iso2)).ToList();
            List<CultureInfo> cultures = CultureInfo.GetCultures(CultureTypes.AllCultures).ToList();
            Logger.Debug("Available cultures for " + pais.Nombre);
            foreach (CultureInfo culture in cultures)
            {
                Logger.Debug(culture.DisplayName + " (" + culture.Name + ")");
            }
            if (cultures.Count > 0)
            {
                this.culture = cultures[0];
            }
        }*/
    }

    public bool is_web()
    {
        return this.parser.GetType().Name.Contains("Html");
    }

    public bool is_mobile()
    {
        Logger.Debug("Request - is_mobile: UA " + this.request.UserAgent);
        if (this.request.UserAgent.Contains("Android") || this.request.UserAgent.Contains("iPhone"))
            return true;
        return false;
    }

    public string getRedirectURL()
    {
        string url = this.redirect;

        if (!url.StartsWith("http"))
        {
            url = ConfigManager.GET("SiteBasePath").ToLower() + url;
        }

        if (this.response.Count > 0)
        {
            string tmp = "";
            foreach (string key in this.response.Keys)
            {
                tmp += String.Format("{0}={1}", key, this.response[key]) + "&";
            }
            tmp = tmp.TrimEnd('&');
            url += (url.Contains('?') ? "&" : "?") + tmp;
        }

        return url;
    }

    public void timer_start(string ev)
    {
        if (!this.timers.ContainsKey(ev))
        {
            Stopwatch t = Stopwatch.StartNew();
            this.timers.Add(ev, t);
        }
    }

    public long timer_stop(string ev)
    {
        if (this.timers.ContainsKey(ev))
        {
            Stopwatch t = this.timers[ev];
            t.Stop();
            Logger.Timing("Tiempo de evento " + ev + ": " + t.ElapsedMilliseconds + "ms");
            return t.ElapsedMilliseconds;
        }

        return 0;
    }

    public Dictionary<string, string> get_array_param(string param)
    {
        Dictionary<string, string> result = new Dictionary<string, string>();
        Match m;

        foreach (string key in this.parameters.Keys)
        {
            if (key.StartsWith(param))
            {
                m = Regex.Match(key, @"^([\d\w_-]+)\[([\d\w_-]+)\]$", RegexOptions.IgnoreCase);
                if (m.Success)
                {
                    if (!result.ContainsKey(m.Groups[2].Value))
                    {
                        result.Add(m.Groups[2].Value, this.parameters[key]);
                    }
                    else
                    {
                        result[m.Groups[2].Value] = this.parameters[key];
                    }
                }
            }
        }

        return result;
    }

    public Dictionary<string, Dictionary<string, string>> get_matrix_param(string param)
    {
        Dictionary<string, Dictionary<string, string>> result = new Dictionary<string, Dictionary<string, string>>();
        Match m;

        foreach (string key in this.parameters.Keys)
        {
            if (key.StartsWith(param))
            {
                m = Regex.Match(key, @"^([\d\w_-]+)\[([\d\w_-]+)\]\[([\d\w_-]+)\]$", RegexOptions.IgnoreCase);
                if (m.Success)
                {
                    if (!result.ContainsKey(m.Groups[2].Value))
                    {
                        result.Add(m.Groups[2].Value, new Dictionary<string, string>());
                    }

                    if (!result[m.Groups[2].Value].ContainsKey(m.Groups[2].Value))
                    {
                        result[m.Groups[2].Value].Add(m.Groups[3].Value, this.parameters[key]);
                    }
                    else
                    {
                        result[m.Groups[2].Value][m.Groups[3].Value] = this.parameters[key];
                    }
                }
            }
        }

        return result;
    }
}