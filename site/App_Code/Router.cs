﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Dynamic;

public class Router
{
    private ArrayList routes;
    private Dictionary<string, string> types;
    private string basePath;
    private RequestWrapper request;


    public Router(RequestWrapper request)
    {
        this.request = request;

        this.types = new Dictionary<string, string>();
        this.types.Add("i", "[0-9]+");
        this.types.Add("a", "[0-9A-Za-z-_]+");
        this.types.Add("h", "[0-9A-Fa-f]+");
        this.types.Add("*", ".?");
        this.types.Add("**", ".+");
        this.types.Add("", @"[^/\.]+");

        this.basePath = "";

        this.routes = new ArrayList();
        this.load();
    }

    private void load()
    {
        dynamic routes = ConfigManager.GetRoutes();
        foreach (dynamic route in routes.route)
        {
            Route objRoute = new Route();
            string[] atribs = { "name", "method", "pattern", "target", "auth", "template" };

            foreach (string attr in atribs)
            {
                if (route.get(attr) != null)
                {
                    objRoute.add(attr, route.get(attr));
                }
            }
            this.routes.Add(objRoute);
        }
    }

    public Route match(string url = null, string method = null)
    {
        if (url == null)
            url = this.request.page.ToLower();

        if (method == null)
            method = this.request.verb;

        foreach (dynamic route in this.routes)
        {
            //Logger.Debug("Route " + route.method + " " + route.pattern);

            String[] methods = route.method.Split('|');
            bool method_match = false;

            for (int j = 0; j < methods.Length; j++)
            {
                if (method.Equals(methods[j], StringComparison.OrdinalIgnoreCase))
                {
                    method_match = true;
                    break;
                }
            }

            if (!method_match)
                continue;

            if (route.pattern.Equals("*"))
            {
                return route;
            }
            else if (route.pattern.Length > 0 && route.pattern.Substring(0, 1).Equals("@"))
            {
                Regex r = new Regex(route.pattern.Substring(1), RegexOptions.IgnoreCase);
                Match m = r.Match(url);

                if (m.Success)
                {
                    //https://msdn.microsoft.com/es-es/library/system.text.regularexpressions.match.groups(v=vs.110).aspx
                    return route;
                }
            }
            else
            {
                //Regex r = new Regex(this.compile(route), RegexOptions.IgnoreCase & RegexOptions.ExplicitCapture);
                //----- compile
                string pattern = "^" + route.pattern + "$";
                Regex r = new Regex(@"(?<pre>/|\.|)\[(?<type>[^:\]]*)(:(?<name>[^:\]]*))?\](?<opt>\?|)");
                MatchCollection ms = r.Matches(pattern);
                List<string> names = new List<string>();

                foreach (Match m in ms)
                {
                    string pre = m.Groups["pre"].Value;
                    pre = pre.Equals(".") ? @"\." : pre;
                    pre = pre.Equals("") ? null : pre;
                    string name = m.Groups["name"].Value;
                    if (!name.Equals(""))
                    {
                        //Logger.Debug("add Route name: " + name);
                        names.Add(name);
                        name = "?<" + name + ">";
                    }
                    string opt = m.Groups["opt"].Value.Equals("") ? null : "?";
                    string type = this.types[m.Groups["type"].Value];
                    string replace = "(" + pre + "(" + name + type + "))" + opt;
                    pattern = pattern.Replace(m.Value, replace);
                }

                r = new Regex(pattern, RegexOptions.IgnoreCase & RegexOptions.ExplicitCapture);
                //----- compile
                Match match = r.Match(url);

                if (match.Success)
                {
                    GroupCollection groups = match.Groups;
                    string[] groupNames = r.GetGroupNames();

                    //if (route.names.Count > 0)
                    if (names.Count > 0)
                    {
                        Dictionary<string, string> parameters = new Dictionary<string, string>();
                        foreach (string groupName in groupNames)
                        {
                            //Logger.Debug("Route parameter: " + groupName + "=" + groups[groupName].Value);
                            //if (route.names.Contains(groupName))
                            if (names.Contains(groupName))
                            {
                                Logger.Debug("add Route parameter: " + groupName + "=" + groups[groupName].Value);
                                parameters.Add(groupName, groups[groupName].Value);
                            }
                        }
                        route.parameters = parameters;
                    }
                    return route;
                }
            }
        }
        return null;
    }

    private string compile(dynamic route)
    {
        string pattern = route.pattern + "$";
        Regex r = new Regex(@"(?<pre>/|\.|)\[(?<type>[^:\]]*)(:(?<name>[^:\]]*))?\](?<opt>\?|)");
        MatchCollection ms = r.Matches(pattern);
        ArrayList names = new ArrayList();

        foreach (Match m in ms)
        {
            string pre = m.Groups["pre"].Value;
            pre = pre.Equals(".") ? @"\." : pre;
            pre = pre.Equals("") ? null : pre;
            string name = m.Groups["name"].Value;
            if (!name.Equals(""))
            {
                Logger.Debug("add Route name: " + name);
                name = "?<" + name + ">";
                names.Add(name);
            }
            string opt = m.Groups["opt"].Value.Equals("") ? null : "?";
            string type = this.types[m.Groups["type"].Value];
            string replace = "(" + pre + "(" + name + type + "))" + opt;
            pattern = pattern.Replace(m.Value, replace);
        }

        route.names = names;
        return pattern;
    }
}

public class Route : DynamicObject
{
    Dictionary<string, object> props;

    public Route()
    {
        this.props = new Dictionary<string, object>();
    }

    public void add(string key, object value)
    {
        this.props.Add(key, value);
    }

    public override bool TryGetMember(GetMemberBinder binder, out object result)
    {
        result = null;
        if (this.props.ContainsKey(binder.Name))
        {
            result = this.props[binder.Name];
        }
        return true;
    }

    public override bool TrySetMember(SetMemberBinder binder, object value)
    {
        this.props[binder.Name] = value;
        return true;
    }
}