﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

/// <summary>
// http://www.asp.net/web-forms/overview/getting-started/getting-started-with-aspnet-45-web-forms/aspnet-error-handling
/// </summary>
public class Logger
{
    static string logFile;
    static string guid;

	private Logger()
	{
    }

    public static void Initialize()
    {
        guid = Guid.NewGuid().ToString();
        logFile = CurrentFile();
    }

    public static string CurrentFile()
    {
        //string folder = HttpContext.Current.Server.MapPath("logs");
        string folder = AppDomain.CurrentDomain.BaseDirectory + "logs";
        StringBuilder stb = new StringBuilder();
        stb.AppendFormat("{0}\\{1}-{2}-{3}-{4}.log", folder, DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), DateTime.Now.ToString("HH"));
        return stb.ToString();
    }

    // Log an Exception
    private static void Log(string type, string msg)
    {
        //format: @date('[d/M/Y:H:i:s]') {$GLOBALS['tracking']} {$_SERVER["PHP_SELF"]}:$line $message
        try
        {
            using (StreamWriter sw = File.AppendText(logFile))
            {
                StringBuilder stb = new StringBuilder();
                stb.AppendFormat("[{0}] {1} {2} {3}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), type, guid, msg);
                string replacement = Regex.Replace(stb.ToString(), @"\t|\n|\r", "");
                sw.WriteLine(replacement);
                sw.Close();
            }
        }
        catch (Exception e)
        {
        }
    }

    public static void Activity(string msg)
    {
        Logger.Log("A", msg);
    }

    public static void Debug(string msg)
    {
        Logger.Log("D", msg);
    }

    public static void Error(string msg)
    {
        Logger.Log("E", msg);
    }

    public static void Db(string msg)
    {
        Logger.Log("S", msg);
    }

    public static void Timing(string msg)
    {
        Logger.Log("T", msg);
    }

    public static void LogProxy(string type, string request)
    {
        //format: @date('[d/M/Y:H:i:s]') {$GLOBALS['tracking']} {$_SERVER["PHP_SELF"]}:$line $message
        try
        {
            string folder = AppDomain.CurrentDomain.BaseDirectory + "logs";
            StringBuilder stb = new StringBuilder();
            stb.AppendFormat("{0}\\proxy-{1}-{2}-{3}-{4}-{5}-{6}-{7}.log", folder, DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), DateTime.Now.ToString("HH"), DateTime.Now.ToString("mm"), DateTime.Now.ToString("ss"), type);
            string file = stb.ToString();
            using (StreamWriter sw = File.AppendText(file))
            {
                sw.WriteLine(request);
                sw.Close();
            }
        }
        catch (Exception e)
        {
            Logger.Error("Error al usar proxy: " + e.Message);
        }
    }
}