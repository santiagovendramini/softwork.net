﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Xml.Serialization;

/// <summary>
/// Summary description for XmlParser
/// </summary>
public class XmlParser : Parser
{
    private RequestWrapper request;

    public XmlParser(RequestWrapper request)
    {
        this.request = request;
    }

    public string encode()
    {
        StringWriter stringwriter = new StringWriter();
        var serializer = new XmlSerializer(this.request.response.GetType());
        serializer.Serialize(stringwriter, this.request.response);
        return stringwriter.ToString();
    }

    public string encode(object encoded)
    {
        StringWriter stringwriter = new StringWriter();
        var serializer = new XmlSerializer(encoded.GetType());
        serializer.Serialize(stringwriter, encoded);
        return stringwriter.ToString();
    }

    public object decode(string encoded)
    {
        StringReader stringReader = new StringReader(encoded);
        XmlSerializer serializer = new XmlSerializer(typeof(object));
        return serializer.Deserialize(stringReader);
    }


    public void render()
    {
        throw new NotImplementedException();
    }

    public void error(string msg)
    {
        throw new NotImplementedException();
    }
}