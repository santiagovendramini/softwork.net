﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Helpers;
using System.Dynamic;
using System.IO;
using System.Text.RegularExpressions;

public class HtmlParser : Parser
{
    private RequestWrapper request;

    public HtmlParser(RequestWrapper request)
    {
        this.request = request;
    }

    public object decode(string encoded)
    {
        return HttpUtility.ParseQueryString(encoded);
    }

    public string encode()
    {
        return "";
    }

    public void render()
    {
        Logger.Debug(String.Format("Rendering HTML page - Status Code: {0} - Redirect: {1}", this.request.status_code, this.request.redirect));

        if (this.request.status_code >= 400)
        {
            this.request.template = "errores/" + this.request.status_code;
            this.request.status_code = 200;
            this.request.status_msg = "OK";
        }

        if (this.request.redirect != null && !this.request.redirect.Equals(""))
        {
            if (this.request.status_code == 200)
            {
                this.request.status_code = 303;
                this.request.status_msg = "See Other";
            }
            this.request.response_obj.Redirect(this.request.getRedirectURL(), false);
            return;
        }

        if (!System.IO.File.Exists(ConfigManager.GET("SiteRoot") + @"sections\" + this.request.template + ".html"))
        {
            Logger.Error("Template no encontrado: sections/" + this.request.template + ".html");
            this.request.template = null;
            throw new Exception("GEN0003");
        }

        if (this.request.template != null)
        {
            TemplateParser parser = new TemplateParser(this.request);
            this.request.response_obj.Write(parser.parseFile(this.request.template, this.request.response));
        }
    }

    public void error(string msg)
    {
        this.request.status_code = 200;
        this.request.status_msg = "OK";
        //this.request.response = new Dictionary<string, object>();
        //buscar mensaje de error

        Regex internal_errors = new Regex(@"^[a-zA-Z]{3}[0-9]{4}$");
        if (internal_errors.IsMatch(msg) && System.IO.File.Exists(ConfigManager.GET("SiteRoot") + @"locale\es_errors.csv"))
        {
            Logger.Debug("Traducir mensaje: " + msg);
            var reader = new StreamReader(File.OpenRead(ConfigManager.GET("SiteRoot") + @"locale\es_errors.csv"));
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(',');
                if (values[0].Equals(msg) && !values[1].Equals(""))
                {
                    //msg = String.Format("({0}) {1}", msg, values[1]);
                    msg = values[1];
                }
            }
        }
        Logger.Error(msg);
        this.request.response.Add("error", msg);
        if (this.request.template == null)
        {
            this.request.template = "error";
        }
        //this.request.redirect = "";
        this.request.render();
    }
}