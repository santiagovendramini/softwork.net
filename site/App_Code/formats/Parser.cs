﻿using System;
using System.Collections.Generic;

public interface Parser
{
    Object decode(string encoded);

    String encode();

    void render();

    void error(string msg);
}