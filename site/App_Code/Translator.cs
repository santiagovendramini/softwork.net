﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

/// <summary>
/// Summary description for Translator
/// </summary>
public class Translator
{
    string idioma;
    string section;
    int counter = 0;

    public Translator(string idioma)
    {
        Logger.Debug("Traductor iniciado para: " + idioma);
        this.idioma = idioma;
    }

	public void cargar_seccion(string section)
	{
        Logger.Debug("Cargando seccion: " + section);
        this.section = section;

        //leer textos de DB
	}

    public string traducir(string code)
    {
        Logger.Debug("Traducir: " + code);
        string path = ConfigManager.GET("SiteRoot") + string.Format("{0}\\{1}.csv", this.idioma.ToLower(), this.section.ToLower());
        using (StreamWriter sw = File.AppendText(path))
        {
            sw.WriteLine(string.Format("WEB_{0}_{1};1;{2};WEB_{0};", this.section.ToUpper(), (++counter), code));
        }
        return code;
    }
}