﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Dynamic;

/// <summary>
/// Summary description for ClientModel
/// </summary>
public class ClientModel : ORM
{
    const string table = "oauth_clients";

    public ClientModel(int id)
        : base(table, false)
    {
        /*
        this.setKey("id");
        List<string> ids = new List<string>();
        ids.Add(id.ToString());
        this.load(ids);
        */
        Dictionary<string, object> filters = new Dictionary<string, object>();
        filters.Add("id", id);
        if (!this.hydrate_fromDB(filters))
        {
            throw new Exception("Cliente no encontrado");
        }
        this.setColumns();
    }

    public ClientModel()
        : base(table, true)
    {
        this.setColumns();
    }

    private ClientModel(bool hydrate = true)
        : base(table, hydrate)
    {
        this.setColumns();
    }

    public static ClientModel fromClientId(string client_id)
    {
        /*
        ClientModel client = new ClientModel();
        client.setKey("client_id");
        List<string> ids = new List<string>();
        ids.Add(client_id);
        if (!client.load(ids))
        {
            return null;
        }
        return client;
        */
        ClientModel client = new ClientModel(false);
        Dictionary<string, object> filters = new Dictionary<string, object>();
        filters.Add("client_id", client_id);
        if (client.hydrate_fromDB(filters))
        {
            return client;
        }
        return null;
    }

    private void setColumns()
    {
        this.keys.Add("id");
        /*
        this.setColumn("id");
        this.setColumn("description");
        this.setColumn("client_id");
        this.setColumn("client_secret");
        this.setColumn("domain");
        this.setColumn("trusted_address");
        this.setColumn("response_url");
        this.setColumn("scope");
        this.setColumn("ttl");
        this.setColumn("comments");
        */
    }
}