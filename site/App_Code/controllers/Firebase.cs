﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for Firebase
/// https://firebase.google.com/docs/cloud-messaging/http-server-ref
/// </summary>
public class Firebase
{
    static string url = "https://fcm.googleapis.com/fcm/send";

    public static void update_token(RequestWrapper request)
    {
        Logger.Debug("Firebase - update_token");

        if (!request.parameters.ContainsKey("token") || request.parameters["token"] == "")
        {
            throw new Exception("Firebase - update_token: token no valido");
        }

        if (request.client == null || request.client.client_id == null)
        {
            throw new Exception("Firebase - update_token: client no valido");
        }

        /*dynamic cu = new ClientsUsers((int)request.client.id, request.user.ANI);
        cu.firebase_token = request.parameters["token"];
        cu.address = request.request.UserHostAddress;
        if(cu.save())
            Logger.Activity("Firebase Token updated correctly");
        else*/
            Logger.Error("Error updating Firebase Token");
    }

    public static void notify_user(RequestWrapper request)
    {
        Logger.Debug("Firebase - notify_user");

        if (request.verb.Contains("POST"))
        {
            if (!request.parameters.ContainsKey("user") || request.parameters["user"].Equals(""))
            {
                Logger.Error("Firebase - notify_user: no se especifica el usuario destino");
                request.response.Add("result", false);
                return;
            }

            if (!request.parameters.ContainsKey("message") || request.parameters["message"].Equals(""))
            {
                Logger.Error("Firebase - notify_user: no se especifica un mensaje");
                request.response.Add("result", false);
                return;
            }

            dynamic cu = ClientsUsers.fromUser(request.parameters["user"]);
            if (cu == null)
            {
                Logger.Error("Firebase - notify_user: no se encontro informacion para el usuario destino");
                request.response.Add("result", false);
                return;
            }

            string token = (string)cu.firebase_token;
            if (token == null || token.Equals(""))
            {
                Logger.Error("Firebase - notify_user: el usuario no posee un token firebase asociado");
                request.response.Add("result", false);
                return;
            }

            request.response.Add("result", Firebase.Send(token, request.parameters["message"]));
        }
    }

    public static bool Send(string destination, string message, Dictionary<string, object> data = null)
	{
        HttpWebResponse response;
        Dictionary<string, object> envelope = new Dictionary<string, object>();
        Dictionary<string, object> notification = new Dictionary<string, object>();

        // collapse_key : 'score_update',
        // time_to_live : 108,
        // delay_while_idle : true,
        notification.Add("body", message);
        notification.Add("title", "BilleteraPaís");
        notification.Add("sound", "default");

        envelope.Add("notification", notification);
        envelope.Add("to", destination);
        if(data != null)
            envelope.Add("data", data);

        JsonParser parser = new JsonParser(null);
        string str_data = parser.encode(envelope);

        try
        {
            //ASCIIEncoding encoding = new ASCIIEncoding();
            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] bytes = encoding.GetBytes(str_data);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = bytes.Length;
            request.Headers.Add("Authorization", "key=" + ConfigManager.GET("FirebaseKey"));

            Stream newStream = request.GetRequestStream();
            newStream.Write(bytes, 0, bytes.Length);
            newStream.Close();

            response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string status = response.StatusCode + " " + response.StatusDescription;
            String soapResponse = reader.ReadToEnd();
            reader.Close();
            response.Close();
            Logger.Activity(status);
            return true;
        }
        catch (WebException ex)
        {
            Logger.Error("Exception: " + ex.Message);
            if (ex.Response != null)
            {
                StreamReader reader = new StreamReader(ex.Response.GetResponseStream());
                Logger.Error(reader.ReadToEnd());
            }
            return false;
        }
	}
}