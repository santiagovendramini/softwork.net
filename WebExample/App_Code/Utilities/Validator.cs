﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;

/// <summary>
/// Summary description for Validator
/// </summary>
public class Validator : VGFramework.Validation.Validator
{
    public enum Type
    {
        Integer,
        Decimal,
        Text

    }
    public Validator()
    {
        //
        // TODO: Add constructor logic here
        //

    }
    public static bool Check(Type type, string value, int min, int max)
    {
        
        switch (type)
        {
            case Type.Integer:
                return Check(MasterType.Integer, value, min, max);
            case Type.Decimal:
                return Check(MasterType.Float, value, min, max);
            case Type.Text:
                return Check(MasterType.Text, value, min, max);
                 
        }
        return false;
    }
}