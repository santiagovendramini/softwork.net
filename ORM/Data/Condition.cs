﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VGFramework.Data
{
    class Condition
    {
        private List<object> fields = new List<object>();
        private string type;
        private string value;

        public Condition(List<object> list, string type = null, string value = null)
        {
            fields = list;
            this.type = type;
            this.value = value;
        }

        public override string ToString()
        {
            switch (type)
            {
                case "when":
                    return "WHEN " + string.Join(" ", fields);
                case "then":
                    return "THEN " + value;
                case "else":
                    return "ELSE " + value;
                default:
                    return string.Join(" ", fields);
            }
        }
    }
}
