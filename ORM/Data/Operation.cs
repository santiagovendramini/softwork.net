﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VGFramework.Data
{
    public class Operation : IStatement
    {
        private List<object> fields = new List<object>();
        private string alias = null;
        private string column1;
        private string column2;
        private Report.OperationType operation;

        public Operation(List<object> list, Report.OperationType operation, string alias)
        {
            fields = list;
            this.alias = alias;
            this.operation = operation;
        }

        public Operation(string column1, string column2, Report.OperationType operation)
        {
            this.column1 = column1;
            this.column2 = column2;
            this.operation = operation;
            fields.Add(ORM.ParseColumnName(column1));
            fields.Add(ORM.ParseColumnName(column2));
        }

        public override string ToString()
        {
            string res = "";
            switch (this.operation)
            {
                case Report.OperationType.SUBTRACTION:
                    res = string.Join(" - ", fields);
                    if (alias != null)
                        res += " AS " + alias;
                    break;
                case Report.OperationType.SUM:
                    res = string.Join(" + ", fields);
                    if (alias != null)
                        res += " AS " + alias;
                    break;
                default:
                    break;
            }
            return res;
        }
    }
}
