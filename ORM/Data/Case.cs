﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VGFramework.Data
{
    public class Case : IStatement
    {
        private string alias = null;
        private List<object> cases = new List<object>();
        public Case(List<object> list, string alias)
        {
            cases = list;
            this.alias = alias;
        }

        public Case()
        {

        }

        public void When(FilterGroup cond, string then)
        {
            if (then.ToString().Contains("'"))
                this.cases.Add("WHEN " + cond.ToString() + " THEN " + then.ToString());
            else
                this.cases.Add("WHEN " + cond.ToString() + " THEN " + new Column(then.ToString()).ToString());
        }

        public void When(FilterGroup cond, int then)
        {
            this.cases.Add("WHEN " + cond.ToString() + " THEN " + then.ToString());
        }

        public void When(FilterGroup cond, string column1, string column2, Report.OperationType operation)
        {
            this.cases.Add("WHEN " + cond.ToString() + "THEN " + new Operation(column1, column2, operation));
        }

        private void When(Filter cond, object then)
        {
            if (then.GetType() == typeof(string))
            {
                if (then.ToString().Contains("'"))
                {
                    this.cases.Add("WHEN " + cond.ToString() + " THEN " + then.ToString());
                }
                else
                {
                    this.cases.Add("WHEN " + cond.ToString() + " THEN " + new Column(then.ToString()).ToString());
                }
            }
            else
            {
                this.cases.Add("WHEN " + cond.ToString() + " THEN " + then.ToString());
            }
        }

        public void When(string column, Report.FilterType type, string value, object then)
        {
            When(new Filter(column, type, value), then);
        }

        public void When(string column, Report.FilterType type, int value, object then)
        {
            When(new Filter(column, type, value), then);
        }

        public void When(string column, Report.FilterType type, double value, object then)
        {
            When(new Filter(column, type, value), then);
        }

        public void Else(object cond)
        {
            if (cond.GetType() == typeof(string))
            {
                if (cond.ToString().Contains("'"))
                {
                    this.cases.Add("ELSE " + cond.ToString());
                }
                else
                {
                    this.cases.Add("ELSE " + new Column(cond.ToString()).ToString());
                }
            }
            else
            {
                this.cases.Add("ELSE " + cond.ToString());
            }
        }

        public override string ToString()
        {
            //return "\nCASE " + string.Join(" ", cases) + " END"; 
            string res = "";
            res = "\nCASE " + string.Join(" ", cases) + " END";
            if (alias != null)
                res += " AS " + alias;
            return res;
        }
    }
}
