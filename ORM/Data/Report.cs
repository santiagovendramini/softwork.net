﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace VGFramework.Data
{
    public class Report : ORM, IStatement
    {
        private List<object> fields = new List<object>();
        private List<object> filters = new List<object>();
        private List<Join> related = new List<Join>();
        private List<string> groups = new List<string>();
        private List<Order> orders = new List<Order>();

        public enum FilterType { LESS, LESS_EQUAL, GREATER, GREATER_EQUAL, REGEXP, POSTFIX, PREFIX, LIKE, NOT_LIKE, BETWEEN, IN, EQUAL, ISNULL, ISNOTNULL, NOT_EQUAL, NOT_IN };
        public enum Connector { AND, OR, NONE };
        public enum JoinType { INNER, LEFT, RIGHT, CROSS };
        public enum OrderType { ASC, DESC };
        public enum OperationType { SUBTRACTION, SUM };
        public enum StatementType { Function, Operation, Case };

        public Report(Manager Manager, string table) : base(Manager, table)
        {
        }

        public Report(Manager Manager, string table, bool hydrate) : base(Manager, table, hydrate)
        {
        }

        public Report Select(Field column)
        {
            this.fields.Add(column.ToString());
            return this;
        }

        public Report Select(string column, string alias = null, string calc = null)
        {
            Select s = new Select(column, alias, calc);
            this.fields.Add(s.ToString());
            return this;
        }
        public Report Select(IStatement column, string alias = null, string calc = null)
        {
            Select s = new Select(column, alias, calc);
            this.fields.Add(s.ToString());
            return this;
        }

        public Report Select(string alias, Report.StatementType type, params object[] args)
        {
            Select s;
            switch (type)
            {
                case StatementType.Function:
                    string name = args[0].ToString();
                    object[] parameters = new object[args.Length - 1];
                    for (int i = 1; i < args.Length; i++)
                    {
                        parameters[i - 1] = args[i];
                    }
                    s = new Select(new Function(name, parameters), alias);
                    this.fields.Add(s.ToString());
                    break;
                case StatementType.Operation:
                    s = new Select(new Operation(args[0].ToString(), args[1].ToString(), (OperationType)args[2]));
                    this.fields.Add(s.ToString());
                    break;
                case StatementType.Case:
                    s = new Select((Case)args[0], alias);
                    this.fields.Add(s.ToString());
                    break;
                default:
                    break;
            }
            return this;
        }

        public Report Filter(string column, Report.FilterType type, int value, Report.Connector connector = Report.Connector.NONE)
        {
            this.filters.Add(new Filter(column, type, value.ToString(), connector));
            return this;
        }

        public Report Filter(string column, Report.FilterType type, double value, Report.Connector connector = Report.Connector.NONE)
        {
            this.filters.Add(new Filter(column, type, value.ToString(), connector));
            return this;
        }

        public Report Filter(string column, Report.FilterType type, string value, Report.Connector connector = Report.Connector.NONE)
        {
            this.filters.Add(new Filter(column, type, value, connector));
            return this;
        }

        public virtual void Filter(string column, Report.FilterType type, string value)
        {
            if (filters.Count > 0)
            {
                Filter last = (Filter)filters[filters.Count - 1];
                last.connector = Connector.AND;
            }
            this.filters.Add(new Filter(column, type, value));
        }

        public Report Filter(object filter)
        {
            this.filters.Add(filter);
            return this;
        }

        public void Relate(object join_table, string join_column, string column, JoinType type, string alias = "")
        {
            if (!column.Contains("."))
                column = table + "." + column;
            if (join_table.GetType() == typeof(string))
            {
                Join j = new Join(join_table.ToString(), join_column, column, type);
                this.related.Add(j);
            }
            if (join_table.GetType() == typeof(Report))
            {
                Join j = new Join((Report)join_table, join_column, column, type, alias);
                this.related.Add(j);
            }
        }

        public void Group(Field group)
        {
            this.groups.Add(group.ToString());
        }

        public void Order(object column, OrderType type)
        {
            Order o = new Order(column, type);
            this.orders.Add(o);
        }

        public virtual void Order(string values)
        {
            foreach (string value in values.Split(','))
            {
                string column = value.Split('.')[0];
                OrderType type;
                switch (value.Split('.')[1])
                {
                    case "desc":
                        type = OrderType.DESC;
                        break;
                    case "asc":
                    default:
                        type = OrderType.ASC;
                        break;
                }
                Order o = new Order(column, type);
                orders.Add(o);
            }
        }

        protected virtual string Select()
        {
            if (this.fields.Count > 0)
            {
                return string.Join(", ", this.fields);
            }
            return "*";
        }

        protected virtual string Filter()
        {
            return string.Join(" ", this.filters);
        }

        protected virtual string Related()
        {
            return string.Join(" ", this.related);
        }

        private string Group()
        {
            return string.Join(", ", this.groups);
        }

        protected virtual string Order()
        {
            if(this.orders.Count > 0)
                return string.Join(", ", this.orders);
            
            if(this.keys.Count > 0)
                return string.Join(", ", this.keys);

            return "";
        }

        public override string ToString()
        {
            string query = string.Format("SELECT {0} \nFROM [{1}] WITH (NOLOCK)", this.Select(), this.table);

            if (this.related.Count > 0)
            {
                query += " \n " + this.Related();
            }

            if (this.filters.Count > 0)
            {
                query += " \nWHERE " + this.Filter();
            }

            if (this.groups.Count > 0)
            {
                query += " \nGROUP BY " + this.Group();
            }

            if (this.orders.Count > 0)
            {
                query += " \nORDER BY " + this.Order();
            }

            return query;
        }

        public DataSet Execute(string adapterAlias = "")
        {
            var ds = new DataSet();
            var command = new SqlCommand(ToString(), Connect());
            var adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
            return ds;

            //using (OleDbConnection con = new OleDbConnection(this.connectionString))
            //{
            //    if (adapterAlias.Equals(""))
            //        adapterAlias = this.table;

            //    DataSet ds = null;
            //    try
            //    {
            //        con.Open();
            //        OleDbDataAdapter a = new OleDbDataAdapter(this.ToString(), con);
            //        ds = new DataSet();
            //        a.Fill(ds, adapterAlias);
            //    }
            //    catch (Exception e)
            //    {
            //        ds = null;
            //    }
            //    return ds;
            //}
        }

        //public DataSet Paginate(int page, int size, string adapterAlias = "")
        //{
        //    using (OleDbConnection con = new OleDbConnection(this.connectionString))
        //    {
        //        if (adapterAlias.Equals(""))
        //            adapterAlias = this.table;

        //        DataSet ds = null;
        //        try
        //        {
        //            string cmd = string.Format("EXEC [VGORM_RunPaginatedQuery] '{0}',{1},{2},'{3}','{4}','{5}','{6}'", this.table, page, size, this.Select().Replace("\"", "''"), this.Filter().Replace("'", "''"), this.Order(), this.Related());
        //            con.Open();
        //            OleDbDataAdapter a = new OleDbDataAdapter(cmd, con);
        //            ds = new DataSet();
        //            a.Fill(ds, adapterAlias);
        //        }
        //        catch (Exception e)
        //        {
        //            ds = null;
        //        }
        //        return ds;
        //    }
        //}

        public DataSet Paginate(int page, int size, string adapterAlias = "")
        {
            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                if (adapterAlias.Equals(""))
                    adapterAlias = this.table;

                DataSet ds = null;
                try
                {
                    string cmd = string.Format("EXEC [VGORM_RunPaginatedQuery] '{0}',{1},{2},'{3}','{4}','{5}','{6}'", this.table, page, size, Select().Replace("\"", "'"), Filter().Replace("'", "''").Replace("\"", "'"), Order().Replace("\"", "'"), Related().Replace("\"", "'"));
                    con.Open();
                    SqlDataAdapter a = new SqlDataAdapter(cmd, con);
                    ds = new DataSet();
                    a.Fill(ds, adapterAlias);
                }
                catch (Exception e)
                {
                    ds = null;
                }
                return ds;
            }
        }
    }
}
