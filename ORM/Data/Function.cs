﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VGFramework.Data
{
    public class Function : IStatement
    {
        private string name;
        private object[] parameters;
        
        public Function(string name, List<object> list)
        {
            this.name = name;
            parameters = list.ToArray();
        }

        public Function(string name, params object[] args)
        {
            this.name = name;
            this.parameters = args;
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i].GetType() == typeof(string))
                {
                    if (args[i].ToString().Contains("."))
                    {
                        args[i] = new Column(args[i].ToString());
                    }
                }
            }
        }

        public override string ToString()
        {
            return name.ToUpper() + "(" + string.Join(", ", parameters) + ")";
        }
    }
}
