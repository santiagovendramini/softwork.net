﻿using System.Collections.Generic;

namespace VGFramework.Data
{
    class Order
    {
        private List<object> fields;
        private string column;
        public Report.OrderType type;

        // debe admitir funciones

        public Order(List<object> list, Report.OrderType type = Report.OrderType.ASC)
        {
            fields = list;
        }

        public Order(object column, Report.OrderType type = Report.OrderType.ASC)
        {
            if (column.GetType() == typeof(string))
            {
                this.column = ORM.ParseColumnName(column.ToString());
            }
            else
            {
                this.column = column.ToString();
            }
            this.type = type;

            fields = new List<object>();
            switch (this.type)
            {
                case Report.OrderType.DESC:
                    fields.Add(this.column + " DESC");
                    break;
                default:
                case Report.OrderType.ASC:
                    fields.Add(this.column + " ASC");
                    break;
            }
        }

        public override string ToString()
        {
            //switch (this.type)
            //{
            //    case Report.OrderType.DESC:
            //        return this.column + " DESC";
            //    default:
            //    case Report.OrderType.ASC:
            //        return this.column + " ASC";
            //}

            return string.Join(", ", fields);
        }
    }
}
