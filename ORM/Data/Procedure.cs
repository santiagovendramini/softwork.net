﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace VGFramework.Data
{
    public class Procedure : ORM
    {
        private string name;
        private List<string> Parameters = new List<string>();

        public Procedure(Manager Manager, string name) : base(Manager, "")
        {
            this.name = name;
        }

        public DataSet Execute()
        {
            var ds = new DataSet();
            var command = new SqlCommand(ToString(), Connect());
            var adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
            return ds;
        }

        public override string ToString()
        {
            string procedure = "";
            procedure = "exec " + name;
            for (int i = 0; i < Parameters.Count; i++)
            {
                procedure += Parameters[i];
            }
            return procedure;
        }

        public void AddParameter(string parameter)
        {
            if (Parameters.Count.Equals(0))
            {
                Parameters.Add(" '" + parameter + "'");
            }
            else
            {
                Parameters.Add(", '" + parameter + "' ");
            }
        }

        public void AddParameter(int parameter)
        {
            if (Parameters.Count.Equals(0))
            {
                Parameters.Add(" " + parameter);
            }
            else
            {
                Parameters.Add(", " + parameter + " ");
            }
        }
    }
}
    