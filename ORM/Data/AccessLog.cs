﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace VGFramework.Data
{
    class AccessLog
    {
        private string requestId;
        public string ip { get; set; }
        public int userId { get; set; }
        public int client { get; set; }
        public string path { get; set; }
        public string parameters { get; set; }
        public string resultCode { get; set; }

        public AccessLog(string guid)
        {
            requestId = guid;
        }

        public void Log()
        {
            Manager manager = new Manager();
            dynamic Log = manager.InternalORM("AccessLog");
            Log.RequestId = requestId;
            if (!Log.Load())
            {
                Log.Date = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                Log.Path = path;
                Log.Params = parameters;
                Log.IP = ip;
            }
            if (userId > 0)
                Log.User = userId;
            if (client > 0)
                Log.Client = client;
            Log.ResultCode = resultCode;
            Log.Save();

            //string query = "INSERT INTO VGF_AccessLog VALUES(@date, @userId, @path, @parameters, @resultCode, @requestId)";
            //using (SqlConnection con = new SqlConnection(manager.GetConfig("ConnectionString")))
            //using (SqlCommand cmd = new SqlCommand(query, con))
            //{
            //    cmd.Parameters.AddWithValue("@date", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            //    cmd.Parameters.AddWithValue("@userId", userId);
            //    cmd.Parameters.AddWithValue("@path", path);
            //    cmd.Parameters.AddWithValue("@parameters", parameters);
            //    cmd.Parameters.AddWithValue("@resultCode", resultCode);
            //    cmd.Parameters.AddWithValue("@requestId", requestId);
            //    con.Open();
            //    cmd.ExecuteNonQuery();
            //    con.Close();
            //}
        }
    }
}
