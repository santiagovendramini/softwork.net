﻿using System;
using System.Linq;
using System.Text;

namespace VGFramework.oAuth
{
    /*
    * Parameters:
    * -	(required) client_id: client identifier
    * -	(optional) state: used for CSRF Protection
    * -	(required) scope: https://tools.ietf.org/html/rfc6749#section-3.3
    * - (required) grant_type, https://tools.ietf.org/html/rfc6749#section-1.3 https://tools.ietf.org/html/rfc6749#section-4:
        * - authorization_code: code for exchange an access_token https://tools.ietf.org/html/rfc6749#section-4.1
        * - implicit: obtain an access_token directly (NOT RECOMENDED) https://tools.ietf.org/html/rfc6749#section-4.2
        * - resource owner password (password): obtain an access_token directly from user/password https://tools.ietf.org/html/rfc6749#section-4.3
        * - client credentials (client): obtain an access_token with client_id and client_secret https://tools.ietf.org/html/rfc6749#section-4.4
    * -	(required) response_type, https://tools.ietf.org/html/rfc6749#section-3.1.1:
        * - code: used for authorization_code grant type.
        * - token: used for implicit grant type.
    * -	(optional) redirect_uri: registered redirection endpoint
    *
    * Success Response:
        * -	code or access_token: authorization_code or access_token
        * - token_type: "bearer" https://tools.ietf.org/html/rfc6749#section-7.1
        * - expires_in: lifetime of access_token, default 3600 secs
        * - refresh_token: https://tools.ietf.org/html/rfc6749#section-6
        * - scope: granted scopes
        * -	(optional) state: request state variable
    *
    * Error Response, https://tools.ietf.org/html/rfc6749#section-5.2:
    * - error:
     * - invalid_request
     * - invalid_client
     * - invalid_grant
     * - unauthorized_client
     * - unsupported_grant_type
     * - invalid_scope
    * - error_description
    * - error_uri
    */
    class Manager
    {
        private static Random random = new Random();

        public static void Validate(OAuthManager manager)
        {
            //string auth = manager.Route.auth.ToString();
            manager.LogActivity("oAuth - Validate");
            string token = manager.QueryParameters.Get("access_token", "").ToString();
            string client_id = manager.QueryParameters.Get("client_id", "").ToString();

            if (token.Equals(""))
            {
                manager.LogError("oAuth - Token no encontrado");
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            //verificar cliente
            if (client_id.Equals(""))
            {
                manager.LogError("oAuth - Client_id requerido");
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            dynamic client = new Client(manager, client_id);
            if (client.Empty())
            {
                manager.LogError("oAuth - client_id no valido " + client_id);
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            manager.Client = client;
            manager.LogActivity("oAuth Client loaded " + manager.Client.Description);

            //verificar scope a nivel cliente
            string[] clientScopes = client.Scope.Split(' ');
            if (!client.Scope.ToString().Equals("All") && !clientScopes.Any(x => x.Equals(manager.Route.name)))
            {
                manager.LogError("oAuth - scope no valido");
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            //verificar Domain
            string domain = manager.RequestHTTP.UserHostAddress;
            if (client.Domain != null && !client.Domain.ToString().Equals("") && client.Domain.Split('/')[0] != domain)
            {
                manager.LogError("oAuth - domain no valido " + domain + " | request " + client.Domain);
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }
        
            dynamic cu = new ClientUser(manager, token, false);
            if (cu.Empty())
            {
                manager.LogError("oAuth - token no encontrado " + token);
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            manager.ClientUser = cu;
            manager.User = manager.InternalORM("Users");
            manager.User.Id = manager.ClientUser.User;
            if (!manager.User.Load())
                throw new Exception("User cannot be loaded");

            //verificar scope a nivel usuario
            string[] userScopes = cu.Scope.Split(' ');
            if (manager.Route.auth.Equals("user") && !cu.Scope.ToString().Equals("All") && !userScopes.Any(x => x.Equals(manager.Route.name)))
            {
                manager.LogError("oAuth - scope no valido");
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            //verificar combinacion cliente-token
            if ((int)client.Id != (int)manager.ClientUser.Client)
            {
                manager.LogError(string.Format("oAuth - access_token (para client_id {1}) no pertenece a client_id {0}", manager.ClientUser.Client, client.Id));
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            //check expiration
            DateTime expiration = DateTime.ParseExact(OAuthManager.Instance.ClientUser.Expiration, OAuthManager.Instance.GetConfig("DateTimeFormat"), null);
            if (expiration < DateTime.Now)
            {
                manager.LogError("oAuth - token expirado " + manager.ClientUser.Expiration.ToString());
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }
        }

        /*
        * Parameters:
        * -	(required) client_id: client identifier
        * -	(optional) state: used for CSRF Protection
        * -	(required) scope: https://tools.ietf.org/html/rfc6749#section-3.3
        * - (required) grant_type, https://tools.ietf.org/html/rfc6749#section-1.3 https://tools.ietf.org/html/rfc6749#section-4:
        *   - authorization_code: code for exchange an access_token https://tools.ietf.org/html/rfc6749#section-4.1
        *   - implicit: obtain an access_token directly (NOT RECOMENDED) https://tools.ietf.org/html/rfc6749#section-4.2
        *   - resource owner password (password): obtain an access_token directly from user/password https://tools.ietf.org/html/rfc6749#section-4.3
        *   - client credentials (client): obtain an access_token with client_id and client_secret https://tools.ietf.org/html/rfc6749#section-4.4
        *   - refresh_token: renew an access_token https://tools.ietf.org/html/rfc6749#section-6
        * -	(required) response_type, https://tools.ietf.org/html/rfc6749#section-3.1.1:
        *   - code: used for authorization_code grant type.
        *   - token: used for implicit grant type.
        *   - refresh_token: used for refresh_token grant type
        * -	(optional) redirect_uri: registered redirection endpoint
        */
        public static void Authorize(OAuthManager manager)
        {
            manager.LogDebug("oAuth.Manager.Login");

            dynamic client = ValidateParameters(manager);
            
            if (manager.isMethod("get"))
            {
                manager.Response.Set("state", manager.QueryParameters.Get("state", ""));
                manager.Response.Set("client_id", manager.QueryParameters.Get("client_id", ""));
                manager.Response.Set("scope", manager.QueryParameters.Get("scope", "All"));
                manager.Response.Set("response_type", manager.QueryParameters.Get("response_type", ""));
                manager.Response.Set("redirect_uri", manager.QueryParameters.Get("redirect_uri", ""));
                manager.Response.Set("grant_type", manager.QueryParameters.Get("grant_type", ""));
            }
            
            if (manager.isMethod("post"))
            {
                string state = manager.Parameters.Get("state", "").ToString();
                string grant_type = manager.Parameters.Get("grant_type", "").ToString();
                switch (grant_type.ToLower())
                {
                    case "authorization_code":
                        AuthorizationCode(manager, client, state);
                        break;
                    case "resource_owner_password":
                    case "client_credentials":
                        throw new Exception("Not Implementd");
                    case "implicit":
                        Implicit(manager, client, state);
                        break;
                    case "refresh_token":
                        Refresh(manager, client);
                        break;
                }
                
                string response_url = client.Response_url.ToString();
                if(!manager.Parameters.Get("redirect_uri", response_url).ToString().Equals(""))
                    response_url = manager.Parameters.Get("redirect_uri", response_url).ToString();
                if (response_url != null && !response_url.Equals("") && grant_type != "refresh_token")
                {
                    manager.Response.Set("redirect", response_url);
                }
            }
        }

        public static void Token(OAuthManager manager)
        {
            string state = manager.Parameters.Get("state", "").ToString();
            string code = manager.Parameters.Get("code", "").ToString();
            string client_id = manager.Parameters.Get("client_id", "").ToString();
            string client_secret = manager.Parameters.Get("client_secret", "").ToString();

            if (client_id.Equals(""))
            {
                manager.LogError("oAuth - client_id no especificado");
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            dynamic client = new Client(manager, client_id);
            if (client.Empty())
            {
                manager.LogError("oAuth - client_id no valido " + client_id);
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            if (client_secret != client.Client_secret)
            {
                manager.LogError("oAuth - Authorization code: client_secret incorrecto");
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            dynamic cu = new ClientUser(manager, code);

            DateTime expiration = DateTime.ParseExact(OAuthManager.Instance.ClientUser.Expiration, OAuthManager.Instance.GetConfig("DateTimeFormat"), null);
            if (expiration < DateTime.Now)
            {
                manager.LogError("oAuth - code expirado " + cu.Expiration.ToString());
                throw new Exception("invalid_request");
            }

            if (code != cu.Code)
            {
                manager.LogError("oAuth - Authorization code: code no coincide");
                throw new Exception("invalid_request");
            }
            
            if (client.Client_id.ToString() != client_id)
            {
                manager.LogError("oAuth - Authorization code: code no pertenece a client_id");
                throw new Exception("invalid request");
            }

            cu.Client = client.Id;
            cu.Code = null;
            cu.Delivered = DateTime.Now;
            cu.Expiration = getExpiration(cu.Delivered.ToString("yyyyMMddHHmmssffff"), client.Ttl.ToString());
            cu.Access_token = RandomString(150);
            cu.Refresh_token = RandomString(150);
            cu.AddKeys();
            if (!cu.Save())
            {
                manager.LogError("oAuth - Authorization code: client_user no guardado");
                throw new Exception("invalid_request");
            }

            manager.Response.Set("client_id", client.Client_id.ToString());
            //manager.Response.Set("delivered", cu.delivered.ToString());
            //manager.Response.Set("expire", cu.expiration.ToString());
            manager.Response.Set("life_time", client.Ttl);
            manager.Response.Set("access_token", cu.Access_token.ToString());
            manager.Response.Set("refresh_token", cu.Refresh_token.ToString());
            manager.Response.Set("token_type", "bearer");
            manager.Response.Set("scope", cu.Scope.ToString());
            manager.Response.Set("state", state);
            manager.Response.Set("redirect", client.Response_url);
        }

        private static Client ValidateParameters(OAuthManager manager)
        {
            string client_id = "";
            string[] requestedScopes = new string[] { "" };
            string uri = "";
            string grant_type = "";
            string response_type = "";

            if (manager.isMethod("get"))
            {
                client_id = manager.QueryParameters.Get("client_id", "");
                requestedScopes = manager.QueryParameters.Get("scope", "All").Split(' ');
                uri = manager.QueryParameters.Get("redirect_uri", "");
                grant_type = manager.QueryParameters.Get("grant_type", "");
                response_type = manager.QueryParameters.Get("response_type", "");
            }

            if (manager.isMethod("post"))
            {
                client_id = manager.Parameters.Get("client_id", "").ToString();
                requestedScopes = manager.Parameters.Get("scope", "All").ToString().Split(' ');
                uri = manager.Parameters.Get("redirect_uri", "").ToString();
                grant_type = manager.Parameters.Get("grant_type", "").ToString();
                response_type = manager.Parameters.Get("response_type", "").ToString();
            }

            //validar cliente (requerido)
            if (client_id.Equals(""))
            {
                manager.LogError("oAuth - client_id no especificado");
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            dynamic client = new Client(manager, client_id);
            if (client.Empty())
            {
                manager.LogError("oAuth - client_id no valido " + client_id);
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            //validar dominio
            string domain = manager.RequestHTTP.UserHostAddress;
            if (client.Domain != null && !client.Domain.ToString().Equals("") && client.Domain.Split('/')[0] != domain)
            {
                manager.LogError("oAuth - domain no valido " + domain + " | request " + client.Domain);
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            //validar scope (requerido)
            string[] scopes = client.Scope.ToString().Split(' ');
            foreach (string scope in requestedScopes)
            {
                if (!scope.Equals("") && !scope.Equals("All") && !scopes.Any(x => x.ToString() == scope))
                {
                    manager.LogError("oAuth - scope no valido");
                    manager.StatusCode = 407;
                    throw new Exception("invalid_request");
                }
            }

            //validar redirect Uri
            Uri requestedRedirectURI = null;
            if (client.Domain != null && !client.Domain.ToString().Equals("") && uri != "")
            {
                requestedRedirectURI = new Uri(uri);
                if (client.Domain.Split('/')[0] != requestedRedirectURI.Host)
                {
                    manager.LogError("oAuth - redirectURI no valido");
                    manager.StatusCode = 407;
                    throw new Exception("invalid_request");
                }
            }

            //validar grant_type (requerido)
            if (grant_type != "authorization_code" && 
                grant_type != "resource_owner_password" &&
                grant_type != "client_credentials" &&
                grant_type != "implicit" &&
                grant_type != "refresh_token")
            {
                manager.LogError("oAuth - grant_type no válido");
                manager.StatusCode = 407;
                throw new Exception("invalid request");
            }

            //validar response_type (requerido)
            if (response_type != "code" && response_type != "token" && response_type != "refresh_token")
            {
                manager.LogError("oAuth - response_type no válido");
                manager.StatusCode = 407;
                throw new Exception("invalid request");
            }

            return client;
        }

        private static void AuthorizationCode(OAuthManager manager, dynamic client, string state)
        {
            manager.LogActivity("oAuth - Authorization-Code");
            string response_type = manager.Parameters.Get("response_type", "code").ToString();

            string user = manager.Parameters.Get("user", "").ToString();
            string password = manager.Parameters.Get("password", "").ToString();

            if (user.Equals(""))
            {
                manager.LogError("oAuth - user no especificado");
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            if (!response_type.Equals("code"))
            {
                manager.LogError("oAuth - Response Type debe ser: code");
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }
            
            dynamic User = manager.InternalORM("Users");
            User.Name = user;
            User.Password = password;
            if (!User.Load())
            {
                manager.LogError("oAuth - Usuario no válido");
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }
            
            dynamic cu = new ClientUser(manager, (int)client.Id, (User.Id).ToString());

            if (!cu.Empty())
            {
                manager.ClientUser = cu;
                //verifico si el code guardado aun esta activo
                DateTime expiration = DateTime.ParseExact(OAuthManager.Instance.ClientUser.Expiration, OAuthManager.Instance.GetConfig("DateTimeFormat"), null);
                if (expiration > DateTime.Now && cu.Code != null)
                {
                    manager.LogDebug("oAuth - Authorization code: code vigente");
                    manager.Response.Set("code", cu.Code.ToString());
                    manager.Response.Set("state", state);
                    return;
                }
            }
            
            cu.Client = client.Id;
            cu.User = (User.Id).ToString();
            cu.Scope = client.Scope;
            cu.Delivered = DateTime.Now;
            cu.Expiration = getExpiration(cu.Delivered.ToString("yyyyMMddHHmmssffff"), "5");
            cu.Code = RandomString(150);
            cu.Access_token = null;
            cu.Refresh_token = null;
            cu.Address = manager.RequestHTTP.UserHostAddress;
            if (!cu.Save())
            {
                manager.LogError("oAuth - Authorization code: client_user no guardado");
                throw new Exception("invalid_request");
            }

            manager.ClientUser = cu;
            manager.Response.Set("code", cu.Code.ToString());
            manager.Response.Set("state", state);
        }

        private static void Implicit(OAuthManager manager, dynamic client, string state)
        {
            manager.LogActivity("oAuth - Implicit");
            string response_type = manager.Parameters.Get("response_type", "token").ToString();

            string user = manager.Parameters.Get("user", "").ToString();
            string password = manager.Parameters.Get("password", "").ToString();

            if (user.Equals(""))
            {
                manager.LogError("oAuth - user no especificado");
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            if (!response_type.Equals("token"))
            {
                manager.LogError("oAuth - Response Type debe ser: token");
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            dynamic User = Helpers.Users.Login(manager, user, password);

            if(User == null)
            {
                manager.LogError("oAuth - Invalid credentials");
                manager.StatusCode = 401;
                throw new Exception("invalid_client");
            }

            dynamic cu = new ClientUser(manager, (int)client.Id, (User.Id).ToString());

            if (!cu.Empty())
            {
                manager.ClientUser = cu;
                //verifico si el token guardado aun esta activo
                DateTime expiration = DateTime.ParseExact(OAuthManager.Instance.ClientUser.Expiration, OAuthManager.Instance.GetConfig("DateTimeFormat"), null);
                if (expiration.AddMinutes(-5) > DateTime.Now && cu.Access_token != null)
                {
                    manager.LogDebug("oAuth - Implicit: token vigente");
                    manager.Response.Set("client_id", client.Client_id.ToString());
                    manager.Response.Set("life_time", client.Ttl);
                    manager.Response.Set("access_token", cu.Access_token.ToString());
                    manager.Response.Set("refresh_token", cu.Refresh_token.ToString());
                    manager.Response.Set("token_type", "bearer");
                    manager.Response.Set("scope", cu.Scope.ToString());
                    manager.Response.Set("state", state);

                    return;
                }
                cu.AddKeys();
            }

            DateTime delivered = DateTime.Now;

            cu.Client = client.Id;
            cu.User = User.Id.ToString();
            cu.Scope = client.Scope;
            cu.Delivered = delivered.ToString(WebManager.Instance.GetConfig("DateTimeFormat"));
            cu.Expiration = getExpiration(delivered.ToString("yyyyMMddHHmmssffff"), client.Ttl.ToString()).ToString(WebManager.Instance.GetConfig("DateTimeFormat"));
            cu.Code = null;
            cu.Access_token = RandomString(150);
            cu.Refresh_token = RandomString(150);
            cu.Address = manager.RequestHTTP.UserHostAddress;

            if (!cu.Save())
            {
                manager.LogError("oAuth - Implicit: client_user no guardado");
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            manager.ClientUser = cu;
            manager.Response.Set("client_id", client.Client_id.ToString());
            //manager.Response.Set("delivered", cu.delivered.ToString());
            //manager.Response.Set("expire", cu.expiration.ToString());
            manager.Response.Set("life_time", client.Ttl);
            manager.Response.Set("access_token", cu.Access_token.ToString());
            manager.Response.Set("refresh_token", cu.Refresh_token.ToString());
            manager.Response.Set("token_type", "bearer");
            manager.Response.Set("scope", cu.Scope.ToString());
            manager.Response.Set("state", state);
        }

        private static void Refresh(OAuthManager manager, dynamic client)
        {
            manager.LogActivity("oAuth - Refresh");
            string state = manager.Parameters.Get("state", "").ToString();
            string token = manager.Parameters.Get("refresh_token", "").ToString();

            if (token.Equals(""))
            {
                manager.LogError("oAuth - Token no encontrado");
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            dynamic cu = new ClientUser(manager, token, true);

            if (cu.Empty())
            {
                manager.LogError("oAuth - token no encontrado " + token);
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            if (!cu.Client.ToString().Equals(client.Id.ToString()))
            {
                manager.LogError("oAuth - Token no corresponde con Cliente");
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }
        
            cu.Client = client.Id;
            cu.User = cu.User;
            cu.Scope = client.Scope;
            cu.Delivered = DateTime.Now;
            cu.Expiration = getExpiration(cu.Delivered.ToString("yyyyMMddHHmmssffff"), client.Ttl.ToString());
            cu.Access_token = RandomString(150);
            cu.Refresh_token = RandomString(150);
            cu.Address = manager.RequestHTTP.UserHostAddress;
            cu.AddKeys();
            if (!cu.Save())
            {
                manager.LogError("oAuth - Refresh: client_user no guardado");
                manager.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            manager.ClientUser = cu;
            manager.Response.Set("client_id", client.Client_id.ToString());
            //manager.Response.Set("delivered", cu.delivered.ToString());
            //manager.Response.Set("expire", cu.expiration.ToString());
            manager.Response.Set("life_time", client.Ttl);
            manager.Response.Set("access_token", cu.Access_token.ToString());
            manager.Response.Set("refresh_token", cu.Refresh_token.ToString());
            manager.Response.Set("token_type", "bearer");
            manager.Response.Set("scope", cu.Scope.ToString());
            manager.Response.Set("state", state);
        }

        private static DateTime getExpiration(string delivered, string ttl)
        {
            DateTime delivered_date = DateTime.ParseExact(delivered, "yyyyMMddHHmmssffff", System.Globalization.CultureInfo.InvariantCulture);
            double ttl_num;
            if (!double.TryParse(ttl, out ttl_num))
            {
                ttl_num = 0;
            }
            return delivered_date.AddMinutes(ttl_num);
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        
        public static string Encrypt(string text)
        {
            return Convert.ToBase64String(Encoding.Unicode.GetBytes(text));
        }

        public static string Decrypt(string text)
        {
            return Encoding.Unicode.GetString(Convert.FromBase64String(text));
        }
    }
}
