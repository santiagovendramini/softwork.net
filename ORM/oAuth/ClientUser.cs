﻿namespace VGFramework.oAuth
{
    public class ClientUser : Data.InternalORM
    {
        public ClientUser(OAuthManager manager, int client, string user) : base(manager, "oAuthClientUsers", true)
        {
            columns["Client"].Value = client;
            columns["User"].Value = user;

            //keys.Add("Client");
            //keys.Add("User");

            Load();
        }

        public ClientUser(OAuthManager manager, string token, bool refresh = false) : base(manager, "oAuthClientUsers", true)
        {
            if(refresh)
                columns["Refresh_token"].Value = token;
            else
                columns["Access_token"].Value = token;

            //keys.Add("Client");
            //keys.Add("User");

            Load();
        }

        public ClientUser(OAuthManager manager, string code) : base(manager, "oAuthClientUsers", true)
        {
            columns["Code"].Value = code;

            //keys.Add("Client");
            //keys.Add("User");

            Load();
        }

        public void AddKeys()
        {
            keys.Add("Client");
            keys.Add("User");
        }
    }
}
