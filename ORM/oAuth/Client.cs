﻿namespace VGFramework.oAuth
{
    internal class Client : Data.InternalORM
    {
        public Client(OAuthManager manager, int id) : base(manager, "oAuthClients", true)
        {
            columns["Id"].Value = id;
            Load();
        }

        public Client(OAuthManager manager, string client_id) : base(manager, "oAuthClients", true)
        {
            columns["Client_id"].Value = client_id;
            Load();
        }
    }
}
