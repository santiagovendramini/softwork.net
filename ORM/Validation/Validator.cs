﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace VGFramework.Validation
{
    public class Validator
    {
        protected enum MasterType
        {
            Integer,
            Email,
            Text,
            IPAddress,
            Option,
            URL,
            MACAddress,
            Float,
            FloatPositive,
            FloatNegative,
            List,
            Regex,
        }




        protected static bool Check(MasterType type, string value, int min, int max)
        {
            Regex validator = null;
            switch (type)
            {
                case MasterType.Integer:
                    validator = new Regex(@"^[0-9]{" + min + "," + max + "}$");
                    break;
                case MasterType.Text:
                    validator = new Regex(@"^[^;%*/()]{" + min + "," + max + "}$");
                    break;
                case MasterType.Float:
                    validator = new Regex(@"^(\-)?\d+(\.\d{" + min + "," + max + "})?$");
                    break;
                case MasterType.FloatPositive:
                    validator = new Regex(@"^\d+(\.\d{" + min + "," + max + "})?$");
                    break;
                case MasterType.FloatNegative:
                    validator = new Regex(@"^\-\d+(\.\d{" + min + "," + max + "})?$");
                    break;
            }

            if (validator != null)
                return validator.IsMatch(value);
            else
                return false;
        }

        protected static bool Check(MasterType type, string value)
        {
            Regex validator = null;
            switch (type)
            {
                case MasterType.Email:
                    validator = new Regex(@"^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$");
                    break;
                case MasterType.Text:
                    validator = new Regex(@"^[^;%*/()]+$");
                    break;
                case MasterType.IPAddress:
                    validator = new Regex(@"^(\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b)$");
                    break;
                case MasterType.MACAddress:
                    validator = new Regex(@"^(?:[a-fA-F0-9]{2}([-:]))(?:[a-fA-F0-9]{2}\1){4}[a-fA-F0-9]{2}$");
                    break;
                case MasterType.URL:
                    validator = new Regex(@"^(ht|f)tps?:\/\/)?([a-zA-Z0-9][a-zA-Z0-9\-]{1,61}[a-zA-Z0-9]\.)*[a-zA-Z0-9][a-zA-Z0-9\-]{1,61}[a-zA-Z0-9](\.[a-zA-Z]{2,})+$");
                    break;
            }

            if (validator != null)
                return validator.IsMatch(value);
            else
                return false;
        }

        protected static bool Check(MasterType type, string value, string[] options)
        {
            Regex validator = null;
            switch (type)
            {
                case MasterType.Option:
                    validator = new Regex(" ^ (" + string.Join("|", options) + ")$");
                    break;
            }

            if (validator != null)
                return validator.IsMatch(value);
            else
                return false;
        }

        protected static bool Check(MasterType type, string value, string regex)
        {
            Regex validator = null;
            switch (type)
            {
                case MasterType.Regex:
                    validator = new Regex(regex);
                    break;
            }

            if (validator != null)
                return validator.IsMatch(value);
            else
                return false;
        }

        protected static bool Check(MasterType type, string value, string item, string glue)
        {
            Regex validator = null;
            switch (type)
            {
                case MasterType.List:
                    validator = new Regex("^" + item + "(" + glue + item + ")*$");
                    break;
            }

            if (validator != null)
                return validator.IsMatch(value);
            else
                return false;
        }
    }
}
