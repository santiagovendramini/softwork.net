﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace VGFramework
{
    class Logger
    {
        private string guid;
        private string file;
        private string dir;
        private string levels;

        public string Levels { set => levels = value; }

        public Logger(string Dir, string Levels)
        {
            dir = Dir;
            levels = Levels;
            guid = Guid.NewGuid().ToString();
            file = GenerateName();

            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }

        public string GetGuid()
        {
            return guid;
        }

        private string GenerateName()
        {
            StringBuilder stb = new StringBuilder();
            stb.AppendFormat("{0}{1}-{2}-{3}-{4}.log", dir, DateTime.Now.ToString("yyyy"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("dd"), DateTime.Now.ToString("HH"));
            return stb.ToString();
        }

        public void Log(string type, string msg)
        {
            if (!levels.Contains(type))
                return;

            //format: @date('[d/M/Y:H:i:s]') {$GLOBALS['tracking']} {$_SERVER["PHP_SELF"]}:$line $message
            try
            {
                using (StreamWriter sw = System.IO.File.AppendText(file))
                {
                    StringBuilder stb = new StringBuilder();
                    stb.AppendFormat("[{0}] {1} {2} {3}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), type, guid, msg);
                    string replacement = Regex.Replace(stb.ToString(), @"\t|\n|\r", "");
                    sw.WriteLine(replacement);
                    sw.Close();
                }
            }
            catch (Exception e)
            {
                //throw new Exception("logger_log_write", e);
                return;
            }
        }
    }
}
