CREATE PROCEDURE [dbo].[VGF_GetMenu]
	@user int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @level int

	SELECT @level = [Level] FROM VGF_Users WHERE [Id] = @user

	SELECT M.*
	FROM [VGF_Menu] M
	LEFT JOIN [VGF_Permissions] P ON P.[Id] = M.[Permission]
	LEFT JOIN [VGF_UserPermissions] UP ON [UP].[Permission] = P.[Id]
	LEFT JOIN [VGF_UserLevelPermissions] ULP ON [ULP].[Permission] = P.[Id]
	WHERE P.[Id] IS NULL
	OR (P.[Id] IS NOT NULL AND ([ULP].[UserLevel] = @level OR [UP].[User] = @user OR ([ULP].[UserLevel] IS NULL AND [UP].[User] IS NULL)))
	ORDER BY M.[FullIndex]
END

