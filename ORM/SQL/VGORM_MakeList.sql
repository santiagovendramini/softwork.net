USE [MakeList]
GO

ALTER TABLE [dbo].[VGF_MakeList] DROP CONSTRAINT [FK_Form_UserLevels]
GO

ALTER TABLE [dbo].[VGF_MakeList] DROP CONSTRAINT [FK_Form_Node]
GO

/****** Object:  Table [dbo].[VGF_MakeList]    Script Date: 12/6/2019 1:10:45 PM ******/
DROP TABLE [dbo].[VGF_MakeList]
GO

/****** Object:  Table [dbo].[VGF_MakeList]    Script Date: 12/6/2019 1:10:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VGF_MakeList](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Root] [bigint] NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[UserLevel] [bigint] NULL,
	[Type] [nvarchar](50) NULL,
	[Comments] [nvarchar](500) NULL,
 CONSTRAINT [PK_Form] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_Structures_Name] UNIQUE NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[VGF_MakeList]  WITH CHECK ADD  CONSTRAINT [FK_Form_Node] FOREIGN KEY([Root])
REFERENCES [dbo].[VGF_MakeListNodes] ([Id])
GO

ALTER TABLE [dbo].[VGF_MakeList] CHECK CONSTRAINT [FK_Form_Node]
GO

ALTER TABLE [dbo].[VGF_MakeList]  WITH CHECK ADD  CONSTRAINT [FK_Form_UserLevels] FOREIGN KEY([UserLevel])
REFERENCES [dbo].[VGF_UserLevels] ([Id])
GO

ALTER TABLE [dbo].[VGF_MakeList] CHECK CONSTRAINT [FK_Form_UserLevels]
GO

