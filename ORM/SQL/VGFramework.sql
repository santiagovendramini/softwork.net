﻿CREATE DATABASE [VGFramework]
GO

USE [VGFramework]
GO

CREATE TABLE [dbo].[Parameters] (
	[key] [nvarchar](50) NOT NULL,
	[value] [nvarchar](300) NOT NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[oAuthClients] (
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[client_id] [nvarchar](max) NOT NULL,
	[client_secret] [nvarchar](max) NOT NULL,
	[domain] [nvarchar](max) NULL,
	[trusted_address] [nvarchar](max) NULL,
	[response_url] [nvarchar](max) NULL,
	[scope] [nvarchar](max) NULL,
	[ttl] [bigint] NOT NULL,
	[comments] [text] NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[oAuthClients] ADD  CONSTRAINT [DF__oAuthClients__ttl] DEFAULT ((60)) FOR [ttl]
GO

CREATE TABLE [dbo].[oAuthClientUsers](
	[client] [bigint] NOT NULL,
	[user] [nvarchar](max) NOT NULL,
	[scope] [nvarchar](max) NULL,
	[code] [nvarchar](max) NULL,
	[access_token] [nvarchar](max) NULL,
	[refresh_token] [nvarchar](max) NULL,
	[delivered] [datetime] NOT NULL,
	[expiration] [datetime] NOT NULL,
	[address] [nvarchar](max) NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[oAuthClientUsers]  WITH CHECK ADD CONSTRAINT [FK_oAuthClientUsers_oAuthClients] FOREIGN KEY([client])
REFERENCES [dbo].[oAuthClients] ([id])
GO

ALTER TABLE [dbo].[oAuthClientUsers] CHECK CONSTRAINT [FK_oAuthClientUsers_oAuthClients]
GO