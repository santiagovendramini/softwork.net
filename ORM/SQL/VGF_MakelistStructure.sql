ALTER PROCEDURE [dbo].[VGF_MakelistStructure]
	@FormName nvarchar(200),
	@level int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @root VARCHAR(11)

	SELECT @root = [Root] FROM [VGF_MakeList] WHERE [Name] LIKE @FormName AND (UserLevel = @level OR UserLevel IS NULL)

	SELECT * FROM [VGF_MakeListNodes] WHERE [FullIndex] LIKE @root OR [FullIndex] LIKE @root + '.%' ORDER BY (LEN([FullIndex]) - LEN(REPLACE([FullIndex],'.',''))), CAST([Order] AS INT)
END