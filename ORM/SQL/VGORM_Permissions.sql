USE [MakeList]
GO

ALTER TABLE [dbo].[VGF_Permissions] DROP CONSTRAINT [FK_Permissions_Permissions1]
GO

/****** Object:  Table [dbo].[VGF_Permissions]    Script Date: 12/6/2019 1:11:20 PM ******/
DROP TABLE [dbo].[VGF_Permissions]
GO

/****** Object:  Table [dbo].[VGF_Permissions]    Script Date: 12/6/2019 1:11:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VGF_Permissions](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[Parent] [bigint] NULL,
	[Path] [nvarchar](500) NULL,
	[Method] [nvarchar](50) NULL,
	[Comments] [nvarchar](500) NULL,
 CONSTRAINT [PK_Permissions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[VGF_Permissions]  WITH CHECK ADD  CONSTRAINT [FK_Permissions_Permissions1] FOREIGN KEY([Parent])
REFERENCES [dbo].[VGF_Permissions] ([Id])
GO

ALTER TABLE [dbo].[VGF_Permissions] CHECK CONSTRAINT [FK_Permissions_Permissions1]
GO

