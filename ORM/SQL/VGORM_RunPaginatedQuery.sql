IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[VGORM_RunPaginatedQuery]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[VGORM_RunPaginatedQuery]
GO

CREATE PROCEDURE [dbo].[VGORM_RunPaginatedQuery]
(
	@table AS NVARCHAR(MAX),
	@pagenum_i AS INT = 1,
	@pagesize_i AS INT = 100,
	@fields AS NVARCHAR(MAX) = '',
	@filter AS NVARCHAR(MAX) = '',
	@sort AS NVARCHAR(MAX) = '',
	@joins AS NVARCHAR(MAX) = ''
)
AS
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @pagenum AS NVARCHAR(MAX)
	DECLARE @pagesize AS NVARCHAR(MAX)

	SELECT @pagenum = CONVERT(VARCHAR, @pagenum_i), @pagesize = CONVERT(VARCHAR, @pagesize_i)

	IF @fields = ''
		SET @fields = '*'

	IF @filter != ''
		SET @filter = ' WHERE ' + @filter
		
	IF @joins = ''
		SET @joins = ' ' + @joins
		
	IF @sort = ''
	BEGIN
		select top 1 @sort = c.name 
		from sys.index_columns ic 
			join sys.indexes i on ic.index_id=i.index_id
			join sys.columns c on c.column_id=ic.column_id
		where 
			i.[object_id] = object_id(@table) and 
			ic.[object_id] = object_id(@table) and 
			c.[object_id] = object_id(@table) and
			is_primary_key = 1
		SET @sort = @table + '.[' + @sort + ']'
	END
	
	IF EXISTS (select * from dbo.sysobjects where id = object_id(N'[dbo].[#tempReport]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
		DROP TABLE #tempReport

	SET @sql = '
		SELECT *
		FROM (
			SELECT TOP (' + @pagenum + '*' + @pagesize + ') ROW_NUMBER() OVER(ORDER BY ' + @sort + ') AS RowNum, ' + @fields + '
			FROM ' + @table + ' 
			' + @joins + ' 
			' + @filter + '
			ORDER BY ' + @sort + '
		) AS aux WHERE RowNum > (' + @pagenum + '-1)*' + @pagesize
	PRINT @sql
	EXEC sp_executesql @sql
END
GO

--EXEC [VGORM_RunPaginatedQuery] 'Integrated', 1, 200, '', '', 'LocalTime'