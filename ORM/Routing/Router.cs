﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace VGFramework.Routing
{
    
    class Router
    {
        protected Dictionary<string, string> types;
        protected ArrayList Routes;

        public Router()
        {
            Routes = new ArrayList();
            LoadDefaultSettings();
        }

        private void LoadDefaultSettings()
        {
            types = new Dictionary<string, string>();
            types.Add("i", "[0-9]+");
            types.Add("a", "[0-9A-Za-z-_]+");
            types.Add("h", "[0-9A-Fa-f]+");
            types.Add("*", ".?");
            types.Add("**", ".+");
            types.Add("", @"[^/\.]+");
        }

        private string Compile(dynamic route)
        {
            string pattern = route.pattern + "$";
            Regex r = new Regex(@"(?<pre>/|\.|)\[(?<type>[^:\]]*)(:(?<name>[^:\]]*))?\](?<opt>\?|)");
            MatchCollection ms = r.Matches(pattern);
            ArrayList names = new ArrayList();

            foreach (Match m in ms)
            {
                string pre = m.Groups["pre"].Value;
                pre = pre.Equals(".") ? @"\." : pre;
                pre = pre.Equals("") ? null : pre;
                string name = m.Groups["name"].Value;
                if (!name.Equals(""))
                {
                    name = "?<" + name + ">";
                    names.Add(name);
                }
                string opt = m.Groups["opt"].Value.Equals("") ? null : "?";
                string type = this.types[m.Groups["type"].Value];
                string replace = "(" + pre + "(" + name + type + "))" + opt;
                pattern = pattern.Replace(m.Value, replace);
            }

            route.names = names;
            return pattern;
        }

        public int Add(Route route)
        {
            return Routes.Add(route);
        }

        public Route Match(string url, string method)
        {
            foreach (dynamic route in Routes)
            {
                string[] methods = route.method.Split('|');
                bool method_match = false;

                for (int j = 0; j < methods.Length; j++)
                {
                    if (method.Equals(methods[j], StringComparison.OrdinalIgnoreCase))
                    {
                        method_match = true;
                        break;
                    }
                }

                if (!method_match)
                    continue;

                if (route.pattern.Equals("*"))
                {
                    return route;
                }
                else if (route.pattern.Length > 0 && route.pattern.Substring(0, 1).Equals("@"))
                {
                    Regex r = new Regex(route.pattern.Substring(1), RegexOptions.IgnoreCase);
                    Match m = r.Match(url);

                    if (m.Success)
                    {
                        //https://msdn.microsoft.com/es-es/library/system.text.regularexpressions.match.groups(v=vs.110).aspx
                        return route;
                    }
                }
                else
                {
                    Regex r = new Regex(route.Compile(types), RegexOptions.IgnoreCase & RegexOptions.ExplicitCapture);
                    Match match = r.Match(url);

                    if (match.Success)
                    {
                        GroupCollection groups = match.Groups;
                        string[] groupNames = r.GetGroupNames();

                        if (route.names.Count > 0)
                        {
                            Dictionary<string, string> parameters = new Dictionary<string, string>();
                            foreach (string groupName in groupNames)
                            {
                                if (route.names.Contains(groupName))
                                {
                                    parameters.Add(groupName, groups[groupName].Value);
                                }
                            }
                            route.parameters = parameters;
                        }
                        return route;
                    }
                }
            }
            return null;
        }
    }

    class Route : DynamicObject
    {
        Dictionary<string, object> props;

        public Route()
        {
            this.props = new Dictionary<string, object>();
        }

        public void add(string key, object value)
        {
            this.props.Add(key, value);
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = null;
            if (this.props.ContainsKey(binder.Name))
            {
                result = this.props[binder.Name];
            }
            return true;
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            this.props[binder.Name] = value;
            return true;
        }

        public string Compile(Dictionary<string, string> types)
        {
            dynamic route = this;
            //string pattern = route.pattern + "$";
            string pattern = "^" + route.pattern + "$";
            Regex r = new Regex(@"(?<pre>/|\.|)\[(?<type>[^:\]]*)(:(?<name>[^:\]]*))?\](?<opt>\?|)");
            MatchCollection ms = r.Matches(pattern);
            ArrayList names = new ArrayList();

            foreach (Match m in ms)
            {
                string pre = m.Groups["pre"].Value;
                pre = pre.Equals(".") ? @"\." : pre;
                pre = pre.Equals("") ? null : pre;
                string name = m.Groups["name"].Value;
                if (!name.Equals(""))
                {
                    names.Add(name);
                    name = "?<" + name + ">";
                }
                string opt = m.Groups["opt"].Value.Equals("") ? null : "?";
                string type = types[m.Groups["type"].Value];
                string replace = "(" + pre + "(" + name + type + "))" + opt;
                pattern = pattern.Replace(m.Value, replace);
            }

            route.names = names;
            return pattern;
        }
    }
}
