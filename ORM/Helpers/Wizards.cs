﻿using System;

namespace VGFramework.Helpers
{
    public class Wizards
    {
        public static void Run(WebManager manager)
        {
            string id = manager.Parameters.Get("wizard", "").ToString();

            if (!Validator.Check(Validator.Type.Integer20, id))
            {
                throw new Exception(ErrorMsg("wizard"));
            }

            string step = manager.Parameters.Get("step", "").ToString();

            if (!Validator.Check(Validator.Type.Integer20, id))
            {
                throw new Exception(ErrorMsg("step"));
            }

            //identify Wizard and step
            //verify permissions
            dynamic wizard = manager.InternalORM("Wizards");
            wizard.id = id;

            if (!wizard.Load())
            {
                throw new Exception(ErrorMsg("not_found"));
            }

            //load step
            dynamic wizard_step = manager.InternalORM("WizardSteps");
            wizard_step.wizard = id;
            wizard_step.step = step;

            if (!wizard_step.Load())
            {
                throw new Exception(ErrorMsg("step_not_found"));
            }

            //change template
            manager.Template = wizard_step.template;

            if (manager.isHTML())
            {
                manager.Response.Set("wizard", wizard.ToDictionary());
                manager.Response.Set("wizard_step", wizard_step.ToDictionary());
            }

            //dispatch
            if (wizard_step.target != null && wizard_step.target != "")
            {
                try
                {
                    manager.LogActivity(string.Format("Execute ({0}) {1}", wizard_step.step, wizard_step.target));
                    manager.Dispatch((string)wizard_step.target);
                }
                catch (Exception e)
                {

                    if (e.InnerException != null)
                    {
                        manager.Response.Set("error", e.InnerException.Message);
                    }
                    else
                    {
                        manager.Response.Set("error", e.Message);
                    }
                }
            }
        }

        protected static string ErrorMsg(string msg)
        {
            return string.Format("wizards_{0}", msg);
        }
    }
}
