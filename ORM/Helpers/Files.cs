﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace VGFramework.Helpers
{
    class Files
    {
        public static void List(OAuthManager Manager)
        {
            Manager.QueryParameters.Get("access_token", "");
            Manager.QueryParameters.Get("client_id", "");

            if (Manager.isMethod("post"))
            {
                List<string> to = Manager.QueryParameters.Get("to", "").Split(',').ToList();
                List<string> names = Manager.QueryParameters.Get("names", "").Split(',').ToList();

                //TODO recibir la entidad relacionada al archivo
                
                Dictionary<string, Dictionary<string, string>> AllFiles = new Dictionary<string, Dictionary<string, string>>();
                for (int i = 0; i < Manager.RequestHTTP.Files.Count; i++)
                {
                    string name = "";
                    string dir = new Manager().GetDirPath("uploadFiles");

                    if (to.ElementAtOrDefault(i) != null && to.ElementAtOrDefault(i) != "")
                        dir = to[i];
                    if (names.ElementAtOrDefault(i) != null && names.ElementAtOrDefault(i) != "")
                        name = names[i];

                    Dictionary<string, string> fileData = ProcessFile(Manager.RequestHTTP.Files[i], dir, name);

                    if (fileData.Count != 0)
                    {
                        if (!AllFiles.Keys.Any(x => x == Manager.RequestHTTP.Files.GetKey(i)))
                            AllFiles.Add(Manager.RequestHTTP.Files.GetKey(i), fileData);
                        else
                            AllFiles.Add(Manager.RequestHTTP.Files.GetKey(i) + AllFiles.Count, fileData);
                    }
                }

                foreach (KeyValuePair<string, Dictionary<string, string>> entry in AllFiles)
                {
                    entry.Value.Add("User", Manager.ClientUser.User.ToString());
                    entry.Value.Add("Id", SaveDB(entry.Value).ToString());
                }

                if (AllFiles.Count > 0)
                    Manager.Response.Set("files", AllFiles);
                else
                    Manager.Response.Set("files", "No se guardaron archivos");
            }

            if (Manager.isMethod("get"))
            {
                Dictionary<string, Dictionary<string, string>> files = new Dictionary<string, Dictionary<string, string>>();
                
                Data.Report report = new Data.Report(Manager, "Files").Filter("Uploaded", Data.Report.FilterType.BETWEEN, Manager.ClientUser.Delivered + "," + Manager.ClientUser.Expiration);
                DataSet ds = report.Execute();

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Dictionary<string, string> file = new Dictionary<string, string>();
                    for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                    {
                        file.Add(ds.Tables[0].Columns[j].ToString(), ds.Tables[0].Rows[i][j].ToString());
                    }
                    files.Add("file" + files.Count, file);
                }

                foreach (KeyValuePair<string, Dictionary<string, string>> entry in files)
                {
                    Manager.Response.Set(entry.Key, entry.Value);
                }
            }
        }

        private static Dictionary<string, string> ProcessFile(HttpPostedFile file, string dir, string name = "")
        {
            Dictionary<string, string> fileData = new Dictionary<string, string>();
            if (name == "")
                name = file.FileName;
            else
                name = name + Path.GetExtension(file.FileName);

            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            var filePath = dir + name;

            if (Path.GetExtension(filePath) != "")
            {
                file.SaveAs(filePath);
                fileData.Add("Path", filePath);
                fileData.Add("Name", name);
                fileData.Add("Extension", Path.GetExtension(filePath));
                fileData.Add("MimeType", file.ContentType);
                fileData.Add("Uploaded", DateTime.Now.ToString());
            }
            return fileData;
        }

        private static int SaveDB(Dictionary<string, string> fileData)
        {
            int id = -1;
            byte[] bytes;
            string path = fileData["Path"];
            bytes = System.IO.File.ReadAllBytes(path);

            string query = "INSERT INTO [VGF_Files] VALUES(@Path, @Name, @Extension, @MimeType, @Uploaded, @User, @Content, @IdAssociated, @Comments) SELECT @@IDENTITY";

            Manager manager = new Manager();
            using (SqlConnection con = new SqlConnection(manager.GetConfig("ConnectionString")))
            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                cmd.Parameters.AddWithValue("@Path", path);
                cmd.Parameters.AddWithValue("@Name", Path.GetFileName(path));
                cmd.Parameters.AddWithValue("@Extension", fileData["Extension"]);
                cmd.Parameters.AddWithValue("@MimeType", fileData["MimeType"]);
                cmd.Parameters.AddWithValue("@Uploaded", fileData["Uploaded"]);
                cmd.Parameters.AddWithValue("@User", fileData["User"]);
                SqlParameter param = cmd.Parameters.Add("@Content", System.Data.SqlDbType.VarBinary);
                cmd.Parameters.AddWithValue("@IdAssociated", "NULL");
                cmd.Parameters.AddWithValue("@Comments", "NULL");
                param.Value = bytes;

                con.Open();
                id = Convert.ToInt32(cmd.ExecuteScalar());
                con.Close();
            }
            return id;
        }

        public static void Delete(OAuthManager Manager)
        {
            string id = Manager.Parameters.Get("id", "").ToString();
            if (id.Equals(""))
                throw new Exception("Debe especificar un archivo");

            if (!int.TryParse(id, out int fileId))
                throw new Exception("El identificador de archivo no es válido");

            dynamic File = Manager.InternalORM("Files");
            File.Id = fileId;
            if (!File.Load())
                throw new Exception("File doesn't exist");
            File.Delete();
        }

        public static void Load(OAuthManager Manager)
        {
            if (Manager.isMethod("get"))
            {
                string Id = Manager.QueryParameters.Get("id", "");
                if (!int.TryParse(Id, out int fileId))
                    throw new Exception("El identificador de archivo no es válido");

                dynamic File = Manager.InternalORM("Files");
                File.Id = fileId;
                if (!File.Load())
                    throw new Exception("File cannot be loaded");
                Manager.Response.Set("Id", File.Id.ToString());
                Manager.Response.Set("Path", File.Path.ToString());
                Manager.Response.Set("Name", File.Name.ToString());
                Manager.Response.Set("Extension", File.Extension.ToString());
                Manager.Response.Set("MimeType", File.MimeType.ToString());
                Manager.Response.Set("Uploaded", File.Uploaded.ToString());
                Manager.Response.Set("User", File.User.ToString());
                Manager.Response.Set("Content", File.Content.ToString());
                Manager.Response.Set("IdAssociated", File.IdAssociated.ToString());
            }
        }

        private static void Move(string pathFrom, string pathTo)
        {
            string dir = pathTo.Replace(Path.GetFileName(pathTo), "");
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            File.Move(pathFrom, pathTo);
        }

        private static bool Validate(string type, HttpPostedFile file)
        {
            bool validate = false;
            switch (type)
            {
                case "audio":
                    if (file.ContentType == "audio/aac" ||
                        file.ContentType == "audio/mpeg" ||
                        file.ContentType == "audio/webm" ||
                        file.ContentType == "audio/ogg" ||
                        file.ContentType == "audio/wav" ||
                        file.ContentType == "audio/midi" ||
                        file.ContentType == "audio/x-wav")
                        validate = true;
                    break;
                case "image":
                    if (file.ContentType == "image/gif" ||
                        file.ContentType == "image/png" ||
                        file.ContentType == "image/jpeg" ||
                        file.ContentType == "image/bmp" ||
                        file.ContentType == "image/webp" ||
                        file.ContentType == "image/svg+xml" ||
                        file.ContentType == "image/tiff" ||
                        file.ContentType == "image/x-icon")
                        validate = true;
                    break;
                case "text":
                    if (file.ContentType == "text/plain" ||
                        file.ContentType == "text/html" ||
                        file.ContentType == "text/css" ||
                        file.ContentType == "text/javascript" ||
                        file.ContentType == "text/csv")
                        validate = true;
                    break;
                case "video":
                    if (file.ContentType == "video/webm" ||
                        file.ContentType == "video/ogg" ||
                        file.ContentType == "video/x-msvideo" ||
                        file.ContentType == "video/mpeg")
                        validate = true;
                    break;
                case "application":
                    if (file.ContentType == "application/octet-stream" ||
                        file.ContentType == "application/pkcs12" ||
                        file.ContentType == "application/vnd.mspowerpoint" ||
                        file.ContentType == "application/xhtml+xml" ||
                        file.ContentType == "application/xml" ||
                        file.ContentType == "application/pdf" ||
                        file.ContentType == "application/x-bzip" ||
                        file.ContentType == "application/x-bzip2" ||
                        file.ContentType == "application/msword" ||
                        file.ContentType == "application/javascript" ||
                        file.ContentType == "application/json" ||
                        file.ContentType == "application/x-rar-compressed" ||
                        file.ContentType == "application/vnd.ms-excel" ||
                        file.ContentType == "application/zip" ||
                        file.ContentType == "application/x-7z-compressed")
                        validate = true;
                    break;
                default:
                    break;
            }
            return validate;
        }
    }
}
