﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VGFramework.Helpers
{
    class MakeListNode
    {
        public string type { get; }
        public string value { get; }
        public string alias { get; set; }

        private Dictionary<string, object> at = new Dictionary<string, object>();
        private List<string> buttons = new List<string>();
        private List<Dictionary<string, object>> options = new List<Dictionary<string, object>>();

        public MakeListNode(string id, string type, string parentId, string value, string name, string order, MakeListNode parent = null, bool hidden = false, bool required = false, bool editable = true, string label = "", string placeholder = "", string mask = "")
        {
            this.type = type;
            this.value = value;
            this.alias = name;
            switch (type)
            {
                case "form":
                    break;
                case "section":
                    at.Add("id", int.Parse(id));
                    at.Add("name", name);
                    if (order != "")
                        at.Add("order", int.Parse(order));
                    break;
                case "button":
                    if (value == "")
                        AddButtons(parent, name);
                    else
                    {
                        at.Add("icon", alias);
                        at.Add("route", value);
                        //at.Add("hidden", hidden);
                    }
                    break;
                case "option":
                    if (order != "")
                        at.Add("order", int.Parse(order));
                    at.Add("name", name);
                    at.Add("value", value);
                    AddOptions(parent, this.Get());
                    break;
                case "data":
                case "datakey":
                case "datamethod":
                case "datavalue":
                    parent.at.Add(type, name);
                    break;
                case "column":
                    at.Add("key", name);
                    at.Add("label", value);
                    at.Add("hidden", hidden);
                    at.Add("required", required);
                    at.Add("editable", editable);
                    break;
                default:
                    if (order != "")
                        at.Add("order", int.Parse(order));
                    at.Add("type", type);
                    at.Add("value", value);
                    at.Add("name", name);
                    at.Add("section", int.Parse(parentId));
                    at.Add("hidden", hidden);
                    at.Add("required", required);
                    at.Add("editable", editable);
                    at.Add("label", label);
                    at.Add("placeholder", placeholder);
                    at.Add("mask", mask);
                    break;
            }
        }

        private void AddOptions(MakeListNode node, Dictionary<string, object> option)
        {
            node.options.Add(option);
        }

        private void AddButtons(MakeListNode node, string button)
        {
            node.buttons.Add(button);
        }

        public Dictionary<string, object> Get()
        {
            if (type != "form" && type != "button" && type != "option" && type != "column")
            {
                at.Add("buttons", buttons);
                at.Add("options", options);
            }
            return at;
        }
    }
}
