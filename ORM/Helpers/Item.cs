﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VGFramework.Helpers
{
    public class Item
    {
        private List<Dictionary<string, object>> childrens = new List<Dictionary<string, object>>();
        private Dictionary<string, object> at = new Dictionary<string, object>();
        public string permission { get; }

        public Item(string permission, string label, string icon, string path, Item parent)
        {
            this.permission = permission;
            at.Add("label", label);
            if (icon != "" && icon != null)
                at.Add("icon", icon);
            if (path != "" && path != null)
                at.Add("to", path);
            if (parent != null)
                AddChildren(parent);
        }

        private void AddChildren(Item parent)
        {
            parent.childrens.Add(this.Get());
        }

        public bool HasChildrens()
        {
            if (childrens.Count > 0)
                return true;
            else
                return false;
        }

        public Dictionary<string, object> Get()
        {
            if (childrens.Count > 0)
                at.Add("childrens", childrens);
            return at;
        }
    }
}
