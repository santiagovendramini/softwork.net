﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VGFramework.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Reflection;

namespace VGFramework.Renders
{
    class XML : Render
    {
       
        public XML()
        {
        }

        public object Decode(string encoded)
        {
            XDocument f = XDocument.Parse(encoded);
            dynamic jsonobj = JObject.Parse(JsonConvert.SerializeXNode(f));

            return jsonobj;
        }
       

        public string Encode(object decoded)
        {
            /*Dictionary<string, dynamic> dic = (Dictionary<string, dynamic>)decoded;
            JObject json=new JObject();
            foreach (var item in dic)
            {
                json.Add(item.Key, item.Value);
            }*/

            string json = System.Web.Helpers.Json.Encode(decoded);
            XNode encode= JsonConvert.DeserializeXNode(json.ToString(), "");
            return encode.ToString();
        }

        public void Error(string msg)
        {
            throw new NotImplementedException();
        }

        public void Render(WebManager Manager)
        {
            Manager.ResponseHTTP.ContentType = "application/xml";
            string res = Encode(Manager.Response.All());
            try
            {
                Manager.ResponseHTTP.Write(res);
                Manager.LogActivity("Response: " + res);
            }
            catch (Exception e)
            {
                Error(e.Message);
            }
        }
    }
}
