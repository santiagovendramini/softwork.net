﻿using Newtonsoft.Json;
using System;
using System.Dynamic;
using Newtonsoft.Json.Linq;

namespace VGFramework.Renders
{
    class Json : Render
    {

        public Json()
        {
        }
        public object Decode(string encoded)
        {
            return JObject.Parse(encoded);
        }

        public string Encode(object decoded)
        {
            return JsonConvert.SerializeObject(decoded);
        }

        public void Error(WebManager Manager, string msg)
        {
            Manager.ResponseHTTP.Write(msg);
        }

        public void Error(string msg)
        {
            throw new NotImplementedException();
        }

        public void Render(WebManager Manager)
        {
            Manager.ResponseHTTP.ContentType = "application/json";
            string res = Encode(Manager.Response.All());
            try
            {
                Manager.ResponseHTTP.Write(res);
            }
            catch (Exception e)
            {
                Error(Manager, e.Message);
            }
        }
    }

    class JsonMemberBinder : GetMemberBinder
    {
        public JsonMemberBinder(string name, bool ignoreCase)
            : base(name, ignoreCase)
        {
        }

        public override DynamicMetaObject FallbackGetMember(DynamicMetaObject target, DynamicMetaObject errorSuggestion)
        {
            throw new NotImplementedException();
        }
    }
}
