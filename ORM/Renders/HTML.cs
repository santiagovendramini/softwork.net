﻿using System;
using System.Web;
using System.Text.RegularExpressions;
using System.IO;
using System.Linq;

namespace VGFramework.Renders
{
    class HTML : Render
    {
        private WebManager manager;
        TemplateParser parser;

        public HTML(WebManager manager)
        {
            this.manager = manager;
            parser = new TemplateParser(manager);
        }

        public object Decode(string encoded)
        {
            return HttpUtility.ParseQueryString(encoded);
        }

        public string Encode(object decoded)
        {
            return "";
        }

        public void Error(string msg)
        {
            Regex internal_errors = new Regex(@"^[a-zA-Z]{3}[0-9]{4}$");
            if (internal_errors.IsMatch(msg) && System.IO.File.Exists(manager.GetConfig("SiteRoot") + @"locale\es_errors.csv"))
            {
                //Logger.Debug("Traducir mensaje: " + msg);
                var reader = new StreamReader(File.OpenRead(manager.GetConfig("SiteRoot") + @"locale\es_errors.csv"));
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    if (values[0].Equals(msg) && !values[1].Equals(""))
                    {
                        //msg = String.Format("({0}) {1}", msg, values[1]);
                        msg = values[1];
                    }
                }
            }

            manager.Response.Set("error", msg);
            if (manager.Template == null)
            {
                manager.Template = "error";
            }

            Render(manager);
        }

        public void Render(WebManager Manager)
        {
            if (manager.StatusCode >= 400)
            {
                manager.Template = "errors/" + manager.StatusCode;
                manager.StatusCode = 200;
            }

            if (!File.Exists(manager.GetConfig("SiteRoot") + @"sections\" + manager.Template + ".html"))
            {
                Manager.LogError("Template no encontrado: sections/" + manager.Template + ".html");
                manager.Template = null;
                throw new Exception("GEN0003");
            }

            if (manager.Template != null)
            {
                manager.ResponseHTTP.Write(parser.parseFile(manager.Template, manager.Response));
            }
        }
    }
}