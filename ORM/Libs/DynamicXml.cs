﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Dynamic;
using System.Xml.Linq;
using Microsoft.CSharp.RuntimeBinder;
using System.Runtime.CompilerServices;

public class DynamicXml : DynamicObject
{
    XElement _root;
    protected DynamicXml(XElement root)
    {
        _root = root;
    }

    public static DynamicXml Parse(string xmlString)
    {
        return new DynamicXml(XDocument.Parse(xmlString).Root);
    }

    public static DynamicXml Load(string filename)
    {
        return new DynamicXml(XDocument.Load(filename).Root);
    }

    public object get(string prop)
    {
        var binder = Binder.GetMember(CSharpBinderFlags.None, prop, this.GetType(),
            new[] { CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, null) });
        var callsite = CallSite<Func<CallSite, object, object>>.Create(binder);
        return callsite.Target(callsite, this);
    }

    public override bool TryGetMember(GetMemberBinder binder, out object result)
    {
        result = null;

        var att = _root.Attribute(binder.Name);
        if (att != null)
        {
            result = att.Value;
            return true;
        }

        var nodes = _root.Elements(binder.Name);
        if (nodes.Count() > 1)
        {
            result = nodes.Select(n => new DynamicXml(n)).ToList();
            return true;
        }

        var node = _root.Element(binder.Name);
        if (node != null)
        {
            if (node.HasElements)
            {
                result = new DynamicXml(node);
            }
            else
            {
                result = node.Value;
            }
            return true;
        }

        return true;
    }
}