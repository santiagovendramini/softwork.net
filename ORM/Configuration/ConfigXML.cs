﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Web;

namespace VGFramework.Configuration
{
    class ConfigXML
    {
        //private static Dictionary<string, string> configs = null;

        //private static void Initialize(bool reload = false)
        //{
        //    if (reload || configs == null)
        //    {
        //        configs = new Dictionary<string, string>();
        //    }
        //}

        private static string ParseFilename(string filename)
        {
            if (filename == null)
            {
                return HttpContext.Current.Request.PhysicalApplicationPath + "config.config";
            }

            return filename;
        }

        public static object Get(string property, string filename = null)
        {
            filename = ParseFilename(filename);

            DynamicXml configs_xml = DynamicXml.Load(filename);
            XMLBinder binder = new XMLBinder(property, false);
            object value;
            if (configs_xml.TryGetMember(binder, out value))
            {
                return value;
            }
            else
            {
                return null;
            }
        }

        public static DynamicXml GetXml(string property, string filename = null)
        {
            object value = Get(property, filename);
            if (value != null)
            {
                return (DynamicXml)value;
            }

            return null;
        }

        public static string GetString(string property, string filename = null)
        {
            object value = Get(property, filename);
            if (value != null)
            {
                return (string)value;
            }

            return null;
        }
    }

    class XMLBinder : GetMemberBinder
    {
        public XMLBinder(string name, bool ignoreCase)
        : base(name, ignoreCase)
        {
        }

        public override DynamicMetaObject FallbackGetMember(DynamicMetaObject target, DynamicMetaObject errorSuggestion)
        {
            throw new NotImplementedException();
        }
    }
}
